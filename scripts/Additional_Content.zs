#loader contenttweaker

import mods.contenttweaker.item.ItemBuilder;
import mods.contenttweaker.item.tool.ItemBuilderTool;

new ItemBuilder().withItemGroup(<itemGroup:create.base>).build("crushed_netherite");
new ItemBuilder().withItemGroup(<itemGroup:create.base>).build("crushed_terminite");
new ItemBuilder().withItemGroup(<itemGroup:create.base>).build("crushed_aeternium");
new ItemBuilder().withItemGroup(<itemGroup:create.base>).build("netherite_nugget");
new ItemBuilder().withItemGroup(<itemGroup:create.base>).build("terminite_nugget");
new ItemBuilder().withItemGroup(<itemGroup:create.base>).build("aeternium_nugget");
