// === Item Tags === //

<tag:items:forge:plates/iron>.remove(<item:mapperbase:iron_plate>);
<tag:items:forge:rods/iron>.remove(<item:mapperbase:iron_rod>);
<tag:items:forge:rods/iron>.add(<item:mcwbridges:iron_rod>);
var IronPlate = <tag:items:forge:plates/iron>.asIIngredient();
var IronRod = <tag:items:forge:rods/iron>.asIIngredient();
var ConsumeWaterBucket = <item:minecraft:water_bucket>.transformReplace(<item:minecraft:bucket>);
<tag:items:minecraft:coals>.remove(<item:mapperbase:bituminous_coal>);
<tag:items:forge:wrenches>.add(<item:wrench:wrench>);
<tag:items:forge:wrenches>.add(<item:create:wrench>);
	// Metal/Steel Doors from EmbellishCraft & Macaw's, so all of them can be used to craft Rusty Doors
<tag:items:embc:heavy_metal_door>.add(<item:embellishcraft:steel_door>);
<tag:items:embc:heavy_metal_door>.add(<item:embellishcraft:white_steel_door>);
<tag:items:embc:heavy_metal_door>.add(<item:mcwdoors:metal_door>);
<tag:items:embc:heavy_metal_door>.add(<item:mcwdoors:metal_reinforced_door>);
<tag:items:embc:sturdy_metal_door>.add(<item:embellishcraft:sturdy_steel_door>);
<tag:items:embc:sturdy_metal_door>.add(<item:embellishcraft:sturdy_white_steel_door>);
<tag:items:embc:warning_metal_door>.add(<item:embellishcraft:warning_steel_door>);
<tag:items:embc:warning_metal_door>.add(<item:embellishcraft:warning_white_steel_door>);
<tag:items:embc:steel_door>.add(<item:embellishcraft:steel_door>);
<tag:items:embc:steel_door>.add(<item:embellishcraft:white_steel_door>);
<tag:items:embc:steel_door>.add(<item:embellishcraft:warning_steel_door>);
<tag:items:embc:steel_door>.add(<item:embellishcraft:warning_white_steel_door>);


// === MapperBase Removal (+Macaw Compat) === //

craftingTable.removeByRegex(".*mapperbase.*");
furnace.removeByRegex(".*mapperbase.*");
blastFurnace.removeByRegex(".*mapperbase.*");
stoneCutter.removeByRegex("mapperbase:concrete.*");
stoneCutter.removeByRegex(".*asphalt.*");
	// Need to restore JUST these two steel recipes, as Steel Nuggets can drop from Tetra's Ancient Ruins
craftingTable.addShapeless("steel_ingot_restoration", <item:mapperbase:steel_ingot>, [<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>,<item:mapperbase:steel_nugget>]);
craftingTable.addShapeless("steel_nugget_restoration", <item:mapperbase:steel_nugget> * 9, [<item:mapperbase:steel_ingot>]);

craftingTable.removeRecipe(<item:embellishcraft:steel_door>);
craftingTable.addShapeless("embc_steeldoor", <item:embellishcraft:steel_door>, [<item:mcwdoors:metal_door>, <tag:items:forge:dyes/light_gray>.asIIngredient()]);
craftingTable.addShapeless("embc_whitesteeldoor_alt", <item:embellishcraft:white_steel_door>, [<item:mcwdoors:metal_door>, <tag:items:forge:dyes/white>.asIIngredient()]);
craftingTable.addShapeless("embc_steeldoor_back2macaw", <item:mcwdoors:metal_door>, [<tag:items:embc:steel_door>.asIIngredient(), <tag:items:forge:dyes/gray>.asIIngredient()]);
craftingTable.removeRecipe(<item:embellishcraft:sturdy_steel_door>);
craftingTable.addShaped("embc_steeldoor_sturdy", <item:embellishcraft:sturdy_steel_door>, [[<item:embellishcraft:steel_door>],[IronPlate]]);
craftingTable.removeRecipe(<item:embellishcraft:sturdy_white_steel_door>);
craftingTable.addShaped("embc_whitesteeldoor_sturdy", <item:embellishcraft:sturdy_white_steel_door>, [[<item:embellishcraft:white_steel_door>],[IronPlate]]);
craftingTable.removeRecipe(<item:embellishcraft:rusty_door>);
craftingTable.addShapeless("embc_rustydoor_withtag", <item:embellishcraft:rusty_door>, [<tag:items:embc:heavy_metal_door>.asIIngredient(),ConsumeWaterBucket]);
craftingTable.removeRecipe(<item:embellishcraft:rusty_door>);
craftingTable.addShapeless("embc_sturdyrustydoor_withtag", <item:embellishcraft:rusty_door>, [<tag:items:embc:sturdy_metal_door>.asIIngredient(),ConsumeWaterBucket]);
craftingTable.removeRecipe(<item:embellishcraft:rusty_door>);
craftingTable.addShapeless("embc_warningrustydoor_withtag", <item:embellishcraft:rusty_door>, [<tag:items:embc:warning_metal_door>.asIIngredient(),ConsumeWaterBucket]);

craftingTable.removeByName("mcwbridges:iron_bridge_middle");
craftingTable.addShaped("mcwb_ironbridge_middle", <item:mcwbridges:most1>, [
	[<item:mcwbridges:iron_armrest>,<item:minecraft:air>,<item:mcwbridges:iron_armrest>],
	[IronPlate,IronPlate,IronPlate]
]);
	// Assorted Steel Sheet/Rod Removals (Replaced with Iron, of course)
craftingTable.removeRecipe(<item:embellishcraft:bulkhead_top>);
craftingTable.addShaped("embc_bulkheadtop", <item:embellishcraft:bulkhead_top> * 2, [[<item:minecraft:light_gray_concrete>],[<item:embellishcraft:bulkhead>]]);
craftingTable.removeRecipe(<item:embellishcraft:locker>);
craftingTable.addShaped("embc_locker", <item:embellishcraft:locker>, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<item:minecraft:air>,IronPlate],
	[IronPlate,<item:minecraft:iron_ingot>,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:steel_rungs>);
craftingTable.addShaped("embc_steelrungs", <item:embellishcraft:steel_rungs> * 3, [
	[IronRod,<item:minecraft:air>,IronRod],
	[<item:minecraft:air>,IronRod,<item:minecraft:air>]
]);
craftingTable.removeRecipe(<item:embellishcraft:steel_wall_ladder>);
craftingTable.addShaped("embc_steelladder", <item:embellishcraft:steel_wall_ladder> * 3, [
	[IronRod,<item:minecraft:air>,IronRod],
	[IronRod,<item:minecraft:iron_ingot>,IronRod],
	[IronRod,<item:minecraft:air>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:steel_terrace_table>);
craftingTable.addShaped("embc_steeltable", <item:embellishcraft:steel_terrace_table>, [
	[IronPlate,IronPlate,IronPlate],
	[<item:minecraft:air>,IronRod,<item:minecraft:air>],
	[<item:minecraft:air>,IronRod,<item:minecraft:air>]
]);
craftingTable.removeRecipe(<item:embellishcraft:steel_terrace_chair>);
craftingTable.addShapedMirrored("embc_steelchair", <item:embellishcraft:steel_terrace_chair>, [
	[IronRod,<item:minecraft:air>,<item:minecraft:air>],
	[IronRod,IronPlate,IronRod],
	[IronRod,<item:minecraft:air>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:steel_suspended_stairs>);
craftingTable.addShapedMirrored("embc_steelstairs", <item:embellishcraft:steel_suspended_stairs> * 4, [
	[<item:minecraft:air>,<item:minecraft:air>,IronPlate],
	[<item:minecraft:air>,IronPlate,IronRod],
	[IronPlate,IronRod,<item:minecraft:air>]
]);
craftingTable.removeRecipe(<item:embellishcraft:steel_beam>);
craftingTable.addShaped("embc_steelbeam", <item:embellishcraft:steel_beam> * 3, [[<item:minecraft:iron_ingot>],[<item:minecraft:iron_ingot>],[<item:minecraft:iron_ingot>]]);
craftingTable.removeRecipe(<item:embellishcraft:bolted_steel_beam>);
craftingTable.addShapeless("embc_steelbeam_bolted", <item:embellishcraft:bolted_steel_beam>, [<item:embellishcraft:steel_beam>,<item:minecraft:iron_nugget>]);
craftingTable.removeRecipe(<item:embellishcraft:bolted_iron_beam>);
craftingTable.addShapeless("embc_ironbeam_bolted", <item:embellishcraft:bolted_iron_beam>, [<item:embellishcraft:iron_beam>,<item:minecraft:iron_nugget>]);
craftingTable.addShapeless("embc_junction2beam_steel", <item:embellishcraft:steel_beam>, [<item:embellishcraft:steel_beam_junction>]);
craftingTable.addShapeless("embc_junction2beam_iron", <item:embellishcraft:iron_beam>, [<item:embellishcraft:iron_beam_junction>]);
craftingTable.addShapeless("embc_bolted2beam_steel", <item:embellishcraft:steel_beam>, [<item:embellishcraft:bolted_steel_beam>,<tag:items:forge:wrenches>.asIIngredient().reuse()]);
craftingTable.addShapeless("embc_bolted2beam_iron", <item:embellishcraft:iron_beam>, [<item:embellishcraft:bolted_iron_beam>,<tag:items:forge:wrenches>.asIIngredient().reuse()]);

craftingTable.removeRecipe(<item:embellishcraft:air_duct>);
craftingTable.addShaped("embc_airduct", <item:embellishcraft:air_duct> * 4, [
	[<item:minecraft:air>,<item:minecraft:iron_ingot>,<item:minecraft:air>],
	[<item:minecraft:iron_ingot>,<item:minecraft:iron_bars>,<item:minecraft:iron_ingot>],
	[<item:minecraft:air>,<item:minecraft:iron_ingot>,<item:minecraft:air>]
]);
craftingTable.removeByName("embellishcraft:rusty_plate");
craftingTable.addShaped("embc_rustyplate", <item:embellishcraft:rusty_plate> * 4, [
	[<item:minecraft:air>,IronPlate,<item:minecraft:air>],
	[IronPlate,ConsumeWaterBucket,IronPlate],
	[<item:minecraft:air>,IronPlate,<item:minecraft:air>]
]);
craftingTable.removeRecipe(<item:embellishcraft:light_metal_floor>);
craftingTable.addShaped("embc_light_metalfloor", <item:embellishcraft:light_metal_floor> * 4, [[IronPlate,IronPlate],[IronPlate,IronPlate]]);
craftingTable.removeRecipe(<item:embellishcraft:dark_metal_floor>);
craftingTable.addShaped("embc_dark_metalfloor", <item:embellishcraft:dark_metal_floor> * 8, [
	[<item:embellishcraft:light_metal_floor>,<item:embellishcraft:light_metal_floor>,<item:embellishcraft:light_metal_floor>],
	[<item:embellishcraft:light_metal_floor>,<tag:items:forge:dyes/black>.asIIngredient(),<item:embellishcraft:light_metal_floor>],
	[<item:embellishcraft:light_metal_floor>,<item:embellishcraft:light_metal_floor>,<item:embellishcraft:light_metal_floor>]
]);
	// Corrugated Metal PLATES (Removal of Steel Sheets from the recipes)
craftingTable.removeRecipe(<item:embellishcraft:black_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_black", <item:embellishcraft:black_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/black>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:blue_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_blue", <item:embellishcraft:blue_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/blue>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:brown_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_brown", <item:embellishcraft:brown_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/brown>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:cyan_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_cyan", <item:embellishcraft:cyan_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/cyan>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:gray_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_gray", <item:embellishcraft:gray_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/gray>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:green_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_green", <item:embellishcraft:green_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/green>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:light_blue_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_light_blue", <item:embellishcraft:light_blue_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/light_blue>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:light_gray_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_light_gray", <item:embellishcraft:light_gray_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/light_gray>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:lime_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_lime", <item:embellishcraft:lime_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/lime>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:magenta_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_magenta", <item:embellishcraft:magenta_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/magenta>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:orange_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_orange", <item:embellishcraft:orange_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/orange>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:pink_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_pink", <item:embellishcraft:pink_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/pink>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:purple_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_purple", <item:embellishcraft:purple_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/purple>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:red_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_red", <item:embellishcraft:red_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/red>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:white_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_white", <item:embellishcraft:white_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/white>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
craftingTable.removeRecipe(<item:embellishcraft:yellow_corrugated_metal_plate>);
craftingTable.addShaped("embc_corrugated_block_yellow", <item:embellishcraft:yellow_corrugated_metal_plate> * 8, [
	[IronPlate,IronPlate,IronPlate],
	[IronPlate,<tag:items:forge:dyes/yellow>.asIIngredient(),IronPlate],
	[IronPlate,IronPlate,IronPlate]
]);
	// Corrugated Metal FENCES (Removal of Steel Rods from the recipes)
craftingTable.removeRecipe(<item:embellishcraft:black_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_black", <item:embellishcraft:black_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:black_corrugated_metal_plate>,IronRod,<item:embellishcraft:black_corrugated_metal_plate>],
	[<item:embellishcraft:black_corrugated_metal_plate>,IronRod,<item:embellishcraft:black_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:blue_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_blue", <item:embellishcraft:blue_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:blue_corrugated_metal_plate>,IronRod,<item:embellishcraft:blue_corrugated_metal_plate>],
	[<item:embellishcraft:blue_corrugated_metal_plate>,IronRod,<item:embellishcraft:blue_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:brown_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_brown", <item:embellishcraft:brown_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:brown_corrugated_metal_plate>,IronRod,<item:embellishcraft:brown_corrugated_metal_plate>],
	[<item:embellishcraft:brown_corrugated_metal_plate>,IronRod,<item:embellishcraft:brown_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:cyan_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_cyan", <item:embellishcraft:cyan_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:cyan_corrugated_metal_plate>,IronRod,<item:embellishcraft:cyan_corrugated_metal_plate>],
	[<item:embellishcraft:cyan_corrugated_metal_plate>,IronRod,<item:embellishcraft:cyan_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:gray_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_gray", <item:embellishcraft:gray_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:gray_corrugated_metal_plate>,IronRod,<item:embellishcraft:gray_corrugated_metal_plate>],
	[<item:embellishcraft:gray_corrugated_metal_plate>,IronRod,<item:embellishcraft:gray_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:green_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_green", <item:embellishcraft:green_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:green_corrugated_metal_plate>,IronRod,<item:embellishcraft:green_corrugated_metal_plate>],
	[<item:embellishcraft:green_corrugated_metal_plate>,IronRod,<item:embellishcraft:green_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:light_blue_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_light_blue", <item:embellishcraft:light_blue_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:light_blue_corrugated_metal_plate>,IronRod,<item:embellishcraft:light_blue_corrugated_metal_plate>],
	[<item:embellishcraft:light_blue_corrugated_metal_plate>,IronRod,<item:embellishcraft:light_blue_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:light_gray_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_light_gray", <item:embellishcraft:light_gray_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:light_gray_corrugated_metal_plate>,IronRod,<item:embellishcraft:light_gray_corrugated_metal_plate>],
	[<item:embellishcraft:light_gray_corrugated_metal_plate>,IronRod,<item:embellishcraft:light_gray_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:lime_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_lime", <item:embellishcraft:lime_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:lime_corrugated_metal_plate>,IronRod,<item:embellishcraft:lime_corrugated_metal_plate>],
	[<item:embellishcraft:lime_corrugated_metal_plate>,IronRod,<item:embellishcraft:lime_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:magenta_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_magenta", <item:embellishcraft:magenta_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:magenta_corrugated_metal_plate>,IronRod,<item:embellishcraft:magenta_corrugated_metal_plate>],
	[<item:embellishcraft:magenta_corrugated_metal_plate>,IronRod,<item:embellishcraft:magenta_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:orange_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_orange", <item:embellishcraft:orange_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:orange_corrugated_metal_plate>,IronRod,<item:embellishcraft:orange_corrugated_metal_plate>],
	[<item:embellishcraft:orange_corrugated_metal_plate>,IronRod,<item:embellishcraft:orange_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:pink_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_pink", <item:embellishcraft:pink_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:pink_corrugated_metal_plate>,IronRod,<item:embellishcraft:pink_corrugated_metal_plate>],
	[<item:embellishcraft:pink_corrugated_metal_plate>,IronRod,<item:embellishcraft:pink_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:purple_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_purple", <item:embellishcraft:purple_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:purple_corrugated_metal_plate>,IronRod,<item:embellishcraft:purple_corrugated_metal_plate>],
	[<item:embellishcraft:purple_corrugated_metal_plate>,IronRod,<item:embellishcraft:purple_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:red_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_red", <item:embellishcraft:red_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:red_corrugated_metal_plate>,IronRod,<item:embellishcraft:red_corrugated_metal_plate>],
	[<item:embellishcraft:red_corrugated_metal_plate>,IronRod,<item:embellishcraft:red_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:white_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_white", <item:embellishcraft:white_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:white_corrugated_metal_plate>,IronRod,<item:embellishcraft:white_corrugated_metal_plate>],
	[<item:embellishcraft:white_corrugated_metal_plate>,IronRod,<item:embellishcraft:white_corrugated_metal_plate>]
]);
craftingTable.removeRecipe(<item:embellishcraft:yellow_corrugated_metal_plate_fence>);
craftingTable.addShaped("embc_corrugated_fence_yellow", <item:embellishcraft:yellow_corrugated_metal_plate_fence> * 6, [
	[<item:embellishcraft:yellow_corrugated_metal_plate>,IronRod,<item:embellishcraft:yellow_corrugated_metal_plate>],
	[<item:embellishcraft:yellow_corrugated_metal_plate>,IronRod,<item:embellishcraft:yellow_corrugated_metal_plate>]
]);
	// Corrugated Metal FENCE GATES (Removal of Steel Rods from the recipes)
craftingTable.removeRecipe(<item:embellishcraft:black_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_black", <item:embellishcraft:black_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:black_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:black_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:blue_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_blue", <item:embellishcraft:blue_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:blue_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:blue_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:brown_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_brown", <item:embellishcraft:brown_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:brown_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:brown_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:cyan_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_cyan", <item:embellishcraft:cyan_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:cyan_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:cyan_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:gray_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_gray", <item:embellishcraft:gray_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:gray_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:gray_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:green_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_green", <item:embellishcraft:green_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:green_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:green_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:light_blue_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_light_blue", <item:embellishcraft:light_blue_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:light_blue_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:light_blue_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:light_gray_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_light_gray", <item:embellishcraft:light_gray_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:light_gray_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:light_gray_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:lime_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_lime", <item:embellishcraft:lime_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:lime_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:lime_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:magenta_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_magenta", <item:embellishcraft:magenta_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:magenta_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:magenta_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:orange_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_orange", <item:embellishcraft:orange_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:orange_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:orange_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:pink_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_pink", <item:embellishcraft:pink_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:pink_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:pink_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:purple_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_purple", <item:embellishcraft:purple_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:purple_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:purple_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:red_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_red", <item:embellishcraft:red_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:red_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:red_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:white_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_white", <item:embellishcraft:white_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:white_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:white_corrugated_metal_plate>,IronRod]
]);
craftingTable.removeRecipe(<item:embellishcraft:yellow_corrugated_metal_plate_fence_gate>);
craftingTable.addShaped("embc_corrugated_fencegate_yellow", <item:embellishcraft:yellow_corrugated_metal_plate_fence_gate> * 6, [
	[IronRod,<item:embellishcraft:yellow_corrugated_metal_plate>,IronRod],
	[IronRod,<item:embellishcraft:yellow_corrugated_metal_plate>,IronRod]
]);
	// EmbellishCraft Crates (Sticks instead of Steel Bolts. Who thought a 4x4 storage block needed such an expensive recipe!?!?)
craftingTable.removeRecipe(<item:embellishcraft:acacia_wooden_crate>);
craftingTable.addShaped("embc_crate_acacia", <item:embellishcraft:acacia_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:acacia_planks>,<item:minecraft:stick>],
	[<item:minecraft:acacia_planks>,<item:minecraft:air>,<item:minecraft:acacia_planks>],
	[<item:minecraft:stick>,<item:minecraft:acacia_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft:birch_wooden_crate>);
craftingTable.addShaped("embc_crate_birch", <item:embellishcraft:birch_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:birch_planks>,<item:minecraft:stick>],
	[<item:minecraft:birch_planks>,<item:minecraft:air>,<item:minecraft:birch_planks>],
	[<item:minecraft:stick>,<item:minecraft:birch_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft:dark_oak_wooden_crate>);
craftingTable.addShaped("embc_crate_darkoak", <item:embellishcraft:dark_oak_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:dark_oak_planks>,<item:minecraft:stick>],
	[<item:minecraft:dark_oak_planks>,<item:minecraft:air>,<item:minecraft:dark_oak_planks>],
	[<item:minecraft:stick>,<item:minecraft:dark_oak_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft:jungle_wooden_crate>);
craftingTable.addShaped("embc_crate_jungle", <item:embellishcraft:jungle_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:jungle_planks>,<item:minecraft:stick>],
	[<item:minecraft:jungle_planks>,<item:minecraft:air>,<item:minecraft:jungle_planks>],
	[<item:minecraft:stick>,<item:minecraft:jungle_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft:oak_wooden_crate>);
craftingTable.addShaped("embc_crate_oak", <item:embellishcraft:oak_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:oak_planks>,<item:minecraft:stick>],
	[<item:minecraft:oak_planks>,<item:minecraft:air>,<item:minecraft:oak_planks>],
	[<item:minecraft:stick>,<item:minecraft:oak_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft:spruce_wooden_crate>);
craftingTable.addShaped("embc_crate_spruce", <item:embellishcraft:spruce_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:spruce_planks>,<item:minecraft:stick>],
	[<item:minecraft:spruce_planks>,<item:minecraft:air>,<item:minecraft:spruce_planks>],
	[<item:minecraft:stick>,<item:minecraft:spruce_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft:warped_wooden_crate>);
craftingTable.addShaped("embc_crate_warped", <item:embellishcraft:warped_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:warped_planks>,<item:minecraft:stick>],
	[<item:minecraft:warped_planks>,<item:minecraft:air>,<item:minecraft:warped_planks>],
	[<item:minecraft:stick>,<item:minecraft:warped_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft:crimson_wooden_crate>);
craftingTable.addShaped("embc_crate_crimson", <item:embellishcraft:crimson_wooden_crate>, [
	[<item:minecraft:stick>,<item:minecraft:crimson_planks>,<item:minecraft:stick>],
	[<item:minecraft:crimson_planks>,<item:minecraft:air>,<item:minecraft:crimson_planks>],
	[<item:minecraft:stick>,<item:minecraft:crimson_planks>,<item:minecraft:stick>]
]);
	// EmbellishCraft Biomes O'Plenty Addon Crates
craftingTable.removeRecipe(<item:embellishcraft-bop:cherry_wooden_crate>);
craftingTable.addShaped("embc_crate_cherry", <item:embellishcraft-bop:cherry_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:cherry_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:cherry_planks>,<item:minecraft:air>,<item:biomesoplenty:cherry_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:cherry_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:dead_wooden_crate>);
craftingTable.addShaped("embc_crate_dead", <item:embellishcraft-bop:dead_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:dead_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:dead_planks>,<item:minecraft:air>,<item:biomesoplenty:dead_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:dead_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:fir_wooden_crate>);
craftingTable.addShaped("embc_crate_fir", <item:embellishcraft-bop:fir_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:fir_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:fir_planks>,<item:minecraft:air>,<item:biomesoplenty:fir_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:fir_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:hellbark_wooden_crate>);
craftingTable.addShaped("embc_crate_hellbark", <item:embellishcraft-bop:hellbark_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:hellbark_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:hellbark_planks>,<item:minecraft:air>,<item:biomesoplenty:hellbark_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:hellbark_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:jacaranda_wooden_crate>);
craftingTable.addShaped("embc_crate_jacaranda", <item:embellishcraft-bop:jacaranda_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:jacaranda_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:jacaranda_planks>,<item:minecraft:air>,<item:biomesoplenty:jacaranda_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:jacaranda_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:magic_wooden_crate>);
craftingTable.addShaped("embc_crate_magic", <item:embellishcraft-bop:magic_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:magic_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:magic_planks>,<item:minecraft:air>,<item:biomesoplenty:magic_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:magic_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:mahogany_wooden_crate>);
craftingTable.addShaped("embc_crate_mahogany", <item:embellishcraft-bop:mahogany_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:mahogany_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:mahogany_planks>,<item:minecraft:air>,<item:biomesoplenty:mahogany_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:mahogany_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:palm_wooden_crate>);
craftingTable.addShaped("embc_crate_palm", <item:embellishcraft-bop:palm_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:palm_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:palm_planks>,<item:minecraft:air>,<item:biomesoplenty:palm_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:palm_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:redwood_wooden_crate>);
craftingTable.addShaped("embc_crate_redwood", <item:embellishcraft-bop:redwood_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:redwood_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:redwood_planks>,<item:minecraft:air>,<item:biomesoplenty:redwood_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:redwood_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:umbran_wooden_crate>);
craftingTable.addShaped("embc_crate_umbran", <item:embellishcraft-bop:umbran_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:umbran_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:umbran_planks>,<item:minecraft:air>,<item:biomesoplenty:umbran_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:umbran_planks>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:embellishcraft-bop:willow_wooden_crate>);
craftingTable.addShaped("embc_crate_willow", <item:embellishcraft-bop:willow_wooden_crate>, [
	[<item:minecraft:stick>,<item:biomesoplenty:willow_planks>,<item:minecraft:stick>],
	[<item:biomesoplenty:willow_planks>,<item:minecraft:air>,<item:biomesoplenty:willow_planks>],
	[<item:minecraft:stick>,<item:biomesoplenty:willow_planks>,<item:minecraft:stick>]
]);

furnace.removeRecipe(<item:embellishcraft:smooth_diorite>);
furnace.removeRecipe(<item:embellishcraft:smooth_granite>);
craftingTable.addShapeless("emb_polished_diorite", <item:minecraft:polished_diorite>, [<item:embellishcraft:smooth_diorite>]);
craftingTable.addShapeless("emb_polished_granite", <item:minecraft:polished_granite>, [<item:embellishcraft:smooth_granite>]);


// === EmbellishCraft Stone Removal === //

craftingTable.removeByRegex(".*embellishcraft.*basalt.*");
craftingTable.removeByRegex(".*embellishcraft.*marble.*");
furnace.removeByRegex(".*embellishcraft.*basalt.*");
furnace.removeByRegex(".*embellishcraft.*marble.*");
stoneCutter.removeByRegex(".*embellishcraft.*basalt.*");
stoneCutter.removeByRegex(".*embellishcraft.*marble.*");

<tag:items:forge:stone>.remove(<item:embellishcraft:basalt>);
<tag:items:forge:stone>.remove(<item:embellishcraft:marble>);
<tag:items:forge:cobblestone>.remove(<item:embellishcraft:basalt_cobblestone>);
<tag:items:forge:cobblestone>.remove(<item:embellishcraft:marble_cobblestone>);
