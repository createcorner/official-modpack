// === Assorted Tweaks === //

craftingTable.removeRecipe(<item:quark:thatch>);
craftingTable.addShapeless("quark_thatch_nerfed", <item:quark:thatch>, [<item:minecraft:wheat>,<item:minecraft:wheat>,<item:minecraft:wheat>,<item:minecraft:wheat>]);
craftingTable.removeByName("quark:building/crafting/thatch_revert");
craftingTable.addShapeless("quark_thatch_reversal_buffed", <item:minecraft:wheat> * 4, [<item:quark:thatch>]);

craftingTable.removeRecipe(<item:quark:iron_plate>);
craftingTable.addShaped("quark_ironplate_nerfed", <item:quark:iron_plate> * 8, [
	[<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>],
	[<item:minecraft:iron_ingot>,<item:minecraft:air>,<item:minecraft:iron_ingot>],
	[<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>]
]);
craftingTable.removeByName("quark:building/crafting/rusty_iron_plate");
craftingTable.addShaped("quark_rustyironplate_nerfed", <item:quark:rusty_iron_plate> * 8, [
	[<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>],
	[<item:minecraft:iron_ingot>,<item:minecraft:water_bucket>.transformReplace(<item:minecraft:bucket>),<item:minecraft:iron_ingot>],
	[<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>,<item:minecraft:iron_ingot>]
]);

craftingTable.removeRecipe(<item:quark:feeding_trough>);
craftingTable.addShaped("quark_feedingtrough_sensible", <item:quark:feeding_trough>, [
	[<tag:items:minecraft:wooden_slabs>.asIIngredient(),<item:minecraft:air>,<tag:items:minecraft:wooden_slabs>.asIIngredient()],
	[<tag:items:minecraft:wooden_slabs>.asIIngredient(),<tag:items:minecraft:wooden_slabs>.asIIngredient(),<tag:items:minecraft:wooden_slabs>.asIIngredient()]
]);


// === Assorted Removal === //

smithing.removeRecipe(<item:quark:flamerang>);
<recipetype:create:crushing>.removeRecipe(<item:quark:biotite>);
<recipetype:create:crushing>.removeRecipe(<item:quark:cactus_paste>);
<recipetype:create:crushing>.addRecipe("crushing_cactus", [<item:minecraft:green_dye>,<item:minecraft:green_dye> % 10], <item:minecraft:cactus>);

campfire.removeRecipe(<item:quark:cooked_crab_leg>);
furnace.removeRecipe(<item:quark:cooked_crab_leg>);
smoker.removeRecipe(<item:quark:cooked_crab_leg>);
campfire.removeRecipe(<item:quark:cooked_frog_leg>);
furnace.removeRecipe(<item:quark:cooked_frog_leg>);
smoker.removeRecipe(<item:quark:cooked_frog_leg>);

craftingTable.removeRecipe(<item:valhelsia_structures:paper_wall>);
<tag:items:forge:glass>.remove(<item:quark:dirty_glass>);
<tag:items:forge:glass_panes>.remove(<item:quark:dirty_glass_pane>);
<tag:items:quark:seed_pouch_holdable>.remove(<item:quark:glowshroom>);

<tag:items:minecraft:leaves>.remove(<item:quark:blue_blossom_leaves>);
<tag:items:minecraft:leaves>.remove(<item:quark:lavender_blossom_leaves>);
<tag:items:minecraft:leaves>.remove(<item:quark:orange_blossom_leaves>);
<tag:items:minecraft:leaves>.remove(<item:quark:pink_blossom_leaves>);
<tag:items:minecraft:leaves>.remove(<item:quark:red_blossom_leaves>);
<tag:items:minecraft:leaves>.remove(<item:quark:yellow_blossom_leaves>);
<tag:items:minecraft:saplings>.remove(<item:quark:blue_blossom_sapling>);
<tag:items:minecraft:saplings>.remove(<item:quark:lavender_blossom_sapling>);
<tag:items:minecraft:saplings>.remove(<item:quark:orange_blossom_sapling>);
<tag:items:minecraft:saplings>.remove(<item:quark:pink_blossom_sapling>);
<tag:items:minecraft:saplings>.remove(<item:quark:red_blossom_sapling>);
<tag:items:minecraft:saplings>.remove(<item:quark:yellow_blossom_sapling>);


// === Wood Removal === //

<tag:items:minecraft:planks>.remove(<item:quark:black_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:blue_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:brown_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:cyan_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:gray_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:green_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:light_blue_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:light_gray_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:lime_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:magenta_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:orange_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:pink_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:purple_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:red_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:white_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:yellow_stained_planks>);
craftingTable.removeByRegex(".*quark.*planks.*");

<tag:items:minecraft:planks>.remove(<item:quark:vertical_acacia_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_birch_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_black_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_blue_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_brown_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_crimson_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_cyan_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_dark_oak_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_gray_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_green_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_jungle_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_light_blue_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_light_gray_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_lime_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_magenta_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_oak_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_orange_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_pink_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_purple_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_red_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_spruce_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_warped_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_white_stained_planks>);
<tag:items:minecraft:planks>.remove(<item:quark:vertical_yellow_stained_planks>);

<tag:items:forge:bookshelves>.remove(<item:quark:acacia_bookshelf>);
<tag:items:forge:bookshelves>.remove(<item:quark:birch_bookshelf>);
<tag:items:forge:bookshelves>.remove(<item:quark:crimson_bookshelf>);
<tag:items:forge:bookshelves>.remove(<item:quark:dark_oak_bookshelf>);
<tag:items:forge:bookshelves>.remove(<item:quark:jungle_bookshelf>);
<tag:items:forge:bookshelves>.remove(<item:quark:spruce_bookshelf>);
<tag:items:forge:bookshelves>.remove(<item:quark:warped_bookshelf>);

	// VANILLA Wooden Chests
craftingTable.removeRecipe(<item:quark:acacia_chest>);
craftingTable.addShaped("quark_ilikewood_chest_acacia", <item:quark:acacia_chest>, [
	[<item:ilikewood:acacia_panels>,<item:ilikewood:acacia_panels>,<item:ilikewood:acacia_panels>],
	[<item:ilikewood:acacia_panels>,<item:minecraft:air>,<item:ilikewood:acacia_panels>],
	[<item:ilikewood:acacia_panels>,<item:ilikewood:acacia_panels>,<item:ilikewood:acacia_panels>]
]);
craftingTable.removeRecipe(<item:quark:birch_chest>);
craftingTable.addShaped("quark_ilikewood_chest_birch", <item:quark:birch_chest>, [
	[<item:ilikewood:birch_panels>,<item:ilikewood:birch_panels>,<item:ilikewood:birch_panels>],
	[<item:ilikewood:birch_panels>,<item:minecraft:air>,<item:ilikewood:birch_panels>],
	[<item:ilikewood:birch_panels>,<item:ilikewood:birch_panels>,<item:ilikewood:birch_panels>]
]);
craftingTable.removeRecipe(<item:quark:crimson_chest>);
craftingTable.addShaped("quark_ilikewood_chest_crimson", <item:quark:crimson_chest>, [
	[<item:ilikewood:crimson_panels>,<item:ilikewood:crimson_panels>,<item:ilikewood:crimson_panels>],
	[<item:ilikewood:crimson_panels>,<item:minecraft:air>,<item:ilikewood:crimson_panels>],
	[<item:ilikewood:crimson_panels>,<item:ilikewood:crimson_panels>,<item:ilikewood:crimson_panels>]
]);
craftingTable.removeRecipe(<item:quark:dark_oak_chest>);
craftingTable.addShaped("quark_ilikewood_chest_dark_oak", <item:quark:dark_oak_chest>, [
	[<item:ilikewood:dark_oak_panels>,<item:ilikewood:dark_oak_panels>,<item:ilikewood:dark_oak_panels>],
	[<item:ilikewood:dark_oak_panels>,<item:minecraft:air>,<item:ilikewood:dark_oak_panels>],
	[<item:ilikewood:dark_oak_panels>,<item:ilikewood:dark_oak_panels>,<item:ilikewood:dark_oak_panels>]
]);
craftingTable.removeRecipe(<item:quark:jungle_chest>);
craftingTable.addShaped("quark_ilikewood_chest_jungle", <item:quark:jungle_chest>, [
	[<item:ilikewood:jungle_panels>,<item:ilikewood:jungle_panels>,<item:ilikewood:jungle_panels>],
	[<item:ilikewood:jungle_panels>,<item:minecraft:air>,<item:ilikewood:jungle_panels>],
	[<item:ilikewood:jungle_panels>,<item:ilikewood:jungle_panels>,<item:ilikewood:jungle_panels>]
]);
craftingTable.removeRecipe(<item:quark:oak_chest>);
craftingTable.addShaped("quark_ilikewood_chest_oak", <item:quark:oak_chest>, [
	[<item:ilikewood:oak_panels>,<item:ilikewood:oak_panels>,<item:ilikewood:oak_panels>],
	[<item:ilikewood:oak_panels>,<item:minecraft:air>,<item:ilikewood:oak_panels>],
	[<item:ilikewood:oak_panels>,<item:ilikewood:oak_panels>,<item:ilikewood:oak_panels>]
]);
craftingTable.removeRecipe(<item:quark:spruce_chest>);
craftingTable.addShaped("quark_ilikewood_chest_spruce", <item:quark:spruce_chest>, [
	[<item:ilikewood:spruce_panels>,<item:ilikewood:spruce_panels>,<item:ilikewood:spruce_panels>],
	[<item:ilikewood:spruce_panels>,<item:minecraft:air>,<item:ilikewood:spruce_panels>],
	[<item:ilikewood:spruce_panels>,<item:ilikewood:spruce_panels>,<item:ilikewood:spruce_panels>]
]);
craftingTable.removeRecipe(<item:quark:warped_chest>);
craftingTable.addShaped("quark_ilikewood_chest_warped", <item:quark:warped_chest>, [
	[<item:ilikewood:warped_panels>,<item:ilikewood:warped_panels>,<item:ilikewood:warped_panels>],
	[<item:ilikewood:warped_panels>,<item:minecraft:air>,<item:ilikewood:warped_panels>],
	[<item:ilikewood:warped_panels>,<item:ilikewood:warped_panels>,<item:ilikewood:warped_panels>]
]);

	// At last, the chest is restored...
craftingTable.addShaped("vanilla_chest_restored", <item:minecraft:chest>, [
	[<tag:items:minecraft:planks>.asIIngredient(),<tag:items:minecraft:planks>.asIIngredient(),<tag:items:minecraft:planks>.asIIngredient()],
	[<tag:items:minecraft:planks>.asIIngredient(),<item:minecraft:air>,<tag:items:minecraft:planks>.asIIngredient()],
	[<tag:items:minecraft:planks>.asIIngredient(),<tag:items:minecraft:planks>.asIIngredient(),<tag:items:minecraft:planks>.asIIngredient()]
]);
craftingTable.addShapeless("vanilla_trappedchest_restored", <item:minecraft:trapped_chest>, [<item:minecraft:chest>,<item:minecraft:tripwire_hook>]);
craftingTable.addShaped("vanilla_ladder_restored", <item:minecraft:ladder> * 3, [
	[<item:minecraft:stick>,<item:minecraft:air>,<item:minecraft:stick>],
	[<item:minecraft:stick>,<item:minecraft:stick>,<item:minecraft:stick>],
	[<item:minecraft:stick>,<item:minecraft:air>,<item:minecraft:stick>]
]);

// === Stone/Brick Removal === //

<tag:items:forge:stone>.remove(<item:quark:basalt>); //voidstone
<tag:items:forge:stone>.remove(<item:quark:polished_basalt>); //voidstone
<tag:items:forge:stone>.remove(<item:quark:brimstone>);
<tag:items:forge:stone>.remove(<item:quark:cobbedstone>);
<tag:items:forge:stone>.remove(<item:quark:limestone>);
<tag:items:forge:stone>.remove(<item:quark:polished_limestone>);
<tag:items:forge:stone>.remove(<item:quark:myalite>);
<tag:items:forge:stone>.remove(<item:quark:polished_myalite>);
<tag:items:forge:stone>.remove(<item:quark:slate>);
<tag:items:forge:stone>.remove(<item:quark:polished_slate>);
<tag:items:forge:stone>.remove(<item:quark:brimstone>);
<tag:items:forge:stone>.remove(<item:quark:cobbedstone>);
<tag:items:forge:cobblestone>.remove(<item:quark:cobbedstone>);

craftingTable.removeByRegex(".*quark.*basalt.*");
stoneCutter.removeByRegex(".*quark.*basalt.*");
craftingTable.removeByRegex(".*quark.*limestone.*");
stoneCutter.removeByRegex(".*quark.*limestone.*");
craftingTable.removeByRegex(".*quark.*myalite.*");
stoneCutter.removeByRegex(".*quark.*myalite.*");
craftingTable.removeByRegex(".*quark.*slate.*");
stoneCutter.removeByRegex(".*quark.*slate.*");

craftingTable.removeRecipe(<item:quark:andesite_bricks>);
craftingTable.removeRecipe(<item:quark:diorite_bricks>);
craftingTable.removeRecipe(<item:quark:granite_bricks>);
