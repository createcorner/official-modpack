// === Item Tags === //

<tag:items:pamhc2:foodsafe_metal>.add(<item:minecraft:iron_ingot>);
<tag:items:pamhc2:foodsafe_metal>.add(<item:create:copper_ingot>);
var PamMetal = <tag:items:pamhc2:foodsafe_metal>.asIIngredient();
<tag:items:forge:milk/small>.add(<item:pamhc2foodcore:freshmilkitem>);
<tag:items:forge:milk/small>.add(<item:pamhc2foodextended:soymilkitem>);
var Milk250mb = <tag:items:forge:milk/small>.asIIngredient();
<tag:items:forge:flour>.add(<item:create:wheat_flour>);
<tag:items:forge:mushrooms>.add(<item:extcaves:mushroom_sweetshroom>);
<tag:items:forge:mushrooms>.remove(<item:quark:glowshroom>);
<tag:items:forge:vegetables>.remove(<item:pamhc2crops:tomatoitem>);
	// NEW Berry Tag, specifically for replacing the Vanilla recipe for Cake
<tag:items:pamhc2:any_berry>.add(<item:minecraft:sweet_berries>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:blackberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:blueberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:cranberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:elderberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:huckleberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:juniperberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:mulberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:raspberryitem>);
<tag:items:pamhc2:any_berry>.add(<item:pamhc2crops:strawberryitem>);
	// BetterAnimalsPlus Compatibility
<tag:items:forge:rawmeats>.add(<item:betteranimalsplus:venisonraw>);
<tag:items:forge:rawchicken>.add(<item:betteranimalsplus:pheasantraw>);
<tag:items:forge:rawchicken>.add(<item:betteranimalsplus:turkey_leg_raw>);
<tag:items:forge:rawchicken>.add(<item:betteranimalsplus:turkey_raw>);
<tag:items:forge:fishes>.add(<item:betteranimalsplus:calamari_raw>);
<tag:items:forge:fishes>.add(<item:betteranimalsplus:crab_meat_raw>);
<tag:items:forge:fishes>.add(<item:betteranimalsplus:eel_meat_raw>);
<tag:items:forge:rawfish>.add(<item:betteranimalsplus:calamari_raw>);
<tag:items:forge:rawfish>.add(<item:betteranimalsplus:crab_meat_raw>);
<tag:items:forge:rawfish>.add(<item:betteranimalsplus:eel_meat_raw>);
<tag:items:forge:stock_ingredients>.add(<item:betteranimalsplus:venisonraw>);
<tag:items:forge:stock_ingredients>.add(<item:betteranimalsplus:pheasantraw>);
<tag:items:forge:stock_ingredients>.add(<item:betteranimalsplus:turkey_leg_raw>);
<tag:items:forge:stock_ingredients>.add(<item:betteranimalsplus:turkey_raw>);
<tag:items:forge:stock_ingredients>.add(<item:betteranimalsplus:calamari_raw>);
<tag:items:forge:stock_ingredients>.add(<item:betteranimalsplus:crab_meat_raw>);
<tag:items:forge:stock_ingredients>.add(<item:betteranimalsplus:eel_meat_raw>);
<tag:items:forge:milk>.add(<item:betteranimalsplus:goatmilk>);
<tag:items:forge:cheese>.add(<item:betteranimalsplus:goatcheese>);
<tag:items:forge:egg>.add(<item:betteranimalsplus:turkey_egg>);
<tag:items:forge:egg>.add(<item:betteranimalsplus:pheasant_egg>);
<tag:items:forge:egg>.add(<item:betteranimalsplus:goose_egg>);
	// Create Machine Duration Constants // #sec div 0.05
var DurationInstant = 20; //1s
var DurationEasiest = 40; //2s
var DurationEasy = 100;	//5s
var DurationAverage = 200; //10s
var DurationHard = 600; //30s
var DurationHardest = 1200; //1m
var DurationExtreme = 2400; //2m


// === Vanilla Recipe Tweaks === //

craftingTable.removeRecipe(<item:minecraft:bread>);
campfire.removeRecipe(<item:minecraft:bread>);
furnace.removeRecipe(<item:minecraft:bread>);
smoker.removeRecipe(<item:minecraft:bread>);
campfire.addRecipe("phc_campfire_dough_bread", <item:minecraft:bread>, <item:pamhc2foodcore:doughitem>, 0.35, DurationAverage);
furnace.addRecipe("phc_cooked_dough_bread", <item:minecraft:bread>, <item:pamhc2foodcore:doughitem>, 0.35, DurationAverage);
smoker.addRecipe("phc_smoked_dough_bread", <item:minecraft:bread>, <item:pamhc2foodcore:doughitem>, 0.35, DurationAverage);

craftingTable.removeRecipe(<item:minecraft:pumpkin_pie>);
craftingTable.addShapeless("phc_pumpkin_pie", <item:minecraft:pumpkin_pie>, [<item:pamhc2foodcore:bakewareitem>.reuse(),<item:minecraft:pumpkin>,<tag:items:forge:egg>.asIIngredient(),<item:minecraft:sugar>,<item:pamhc2foodcore:doughitem>]);
craftingTable.removeRecipe(<item:minecraft:cookie>);
craftingTable.addShapeless("phc_cookies", <item:minecraft:cookie> * 8, [<item:pamhc2foodcore:bakewareitem>.reuse(),<item:pamhc2foodcore:chocolatebaritem>,<item:minecraft:sugar>,<item:pamhc2foodcore:doughitem>]);
craftingTable.removeRecipe(<item:minecraft:cake>);
craftingTable.addShapeless("phc_vanillacake", <item:minecraft:cake>, [<item:pamhc2foodcore:bakewareitem>.reuse(),<item:minecraft:sugar>,<tag:items:forge:batter>.asIIngredient(),<tag:items:pamhc2:any_berry>.asIIngredient()]);


// === Pam's Harvestcraft Tweaks + Create Compat === //

craftingTable.removeRecipe(<item:pamhc2foodcore:cuttingboarditem>);
craftingTable.addShapedMirrored("phc_tool_cuttingboard", <item:pamhc2foodcore:cuttingboarditem>, [
	[PamMetal,<item:minecraft:air>,<item:minecraft:air>],
	[<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>],
	[<item:minecraft:air>,<item:minecraft:air>,<tag:items:minecraft:planks>]
]);

craftingTable.removeRecipe(<item:pamhc2foodcore:juiceritem>);
craftingTable.addShaped("phc_tool_juicer", <item:pamhc2foodcore:juiceritem>, [
	[<item:minecraft:air>,PamMetal,<item:minecraft:air>],
	[PamMetal,PamMetal,PamMetal]
]);

craftingTable.removeRecipe(<item:pamhc2foodcore:potitem>);
craftingTable.addShapedMirrored("phc_tool_pot", <item:pamhc2foodcore:potitem>, [
	[<item:minecraft:stick>,PamMetal,PamMetal],
	[<item:minecraft:air>,PamMetal,PamMetal]
]);

craftingTable.removeRecipe(<item:pamhc2foodcore:saucepanitem>);
craftingTable.addShapedMirrored("phc_tool_saucepan", <item:pamhc2foodcore:saucepanitem>, [
	[<item:minecraft:stick>,PamMetal]
]);

craftingTable.removeRecipe(<item:pamhc2foodcore:skilletitem>);
craftingTable.addShapedMirrored("phc_tool_skillet", <item:pamhc2foodcore:skilletitem>, [
	[PamMetal,<item:minecraft:air>,<item:minecraft:air>],
	[<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>],
	[<item:minecraft:air>,<item:minecraft:air>,<item:minecraft:stick>]
]);

craftingTable.removeRecipe(<item:pamhc2foodcore:chocolatebaritem>);
craftingTable.addShapeless("phc_milk_chocolatebar", <item:pamhc2foodcore:chocolatebaritem>, [<item:pamhc2foodcore:saucepanitem>.reuse(),<item:minecraft:cocoa_beans>,<item:minecraft:sugar>,<item:pamhc2foodcore:freshmilkitem>]);
<recipetype:create:compacting>.removeRecipe(<item:create:bar_of_chocolate>);
<recipetype:create:compacting>.addRecipe("phc_compact_chocolate", "none", <item:pamhc2foodcore:chocolatebaritem> * 2, [], [<fluid:create:chocolate> * 250], DurationAverage);

<recipetype:create:emptying>.addRecipe("phc_freshwater_draining", <item:minecraft:air>, <fluid:minecraft:water> * 125, <item:pamhc2foodcore:freshwateritem>, DurationInstant);
<recipetype:create:emptying>.addRecipe("phc_freshmilk_draining", <item:minecraft:air>, <fluid:create:milk> * 125, Milk250mb, DurationInstant);

	// Create's Mixer as an automated substitute for the Mixing Bowl
<recipetype:create:mixing>.addRecipe("phc_automix_icecream", "none", <item:pamhc2foodcore:icecreamitem>, [<item:pamhc2foodcore:saltitem>,<tag:items:forge:ices>.asIIngredient()], [<fluid:create:milk> * 125]);
//<recipetype:create:mixing>.addRecipe("phc_automix_icecream_nofluid", "none", <item:pamhc2foodcore:icecreamitem>, [Milk250mb,<item:pamhc2foodcore:saltitem>,<tag:items:forge:ices>.asIIngredient()]);
<recipetype:create:mixing>.addRecipe("phc_automix_icecream_chocolate", "none", <item:pamhc2foodcore:chocolateicecreamitem>, [<item:pamhc2foodcore:icecreamitem>,<item:pamhc2foodcore:cocoapowderitem>]);
<recipetype:create:mixing>.addRecipe("phc_automix_icecream_caramel", "none", <item:pamhc2foodcore:caramelicecreamitem>, [<item:pamhc2foodcore:icecreamitem>,<item:pamhc2foodcore:caramelitem>]);
<recipetype:create:mixing>.addRecipe("phc_automix_saltandpepper", "none", <item:pamhc2foodextended:saltandpepperitem>, [<item:pamhc2foodcore:saltitem>,<item:pamhc2foodextended:blackpepperitem>]);
<recipetype:create:mixing>.addRecipe("phc_automix_batter", "none", <item:pamhc2foodcore:batteritem> * 2, [<tag:items:forge:flour>.asIIngredient(),<tag:items:forge:egg>.asIIngredient()], [<fluid:create:milk> * 125]);
//<recipetype:create:mixing>.addRecipe("phc_automix_batter_nofluid", "none", <item:pamhc2foodcore:batteritem> * 2, [<tag:items:forge:flour>.asIIngredient(),Milk250mb,<tag:items:forge:egg>.asIIngredient()]);
<recipetype:create:mixing>.addRecipe("phc_automix_cottoncandy", "none", <item:pamhc2foodcore:cottoncandyitem>, [<tag:items:forge:sugar>.asIIngredient(),<tag:items:forge:dyes>.asIIngredient()]);
<recipetype:create:mixing>.addRecipe("phc_automix_marzipan", "heated", <item:pamhc2foodextended:marzipanitem>, [<item:pamhc2trees:almonditem>], [<fluid:create:honey> * 250]);
//<recipetype:create:mixing>.addRecipe("phc_automix_marzipan_nofluid", "heated", <item:pamhc2foodextended:marzipanitem>, [<item:pamhc2trees:almonditem>,<item:minecraft:honey_bottle>]);
<recipetype:create:mixing>.addRecipe("phc_automix_sausage", "heated", <item:pamhc2foodextended:sausageitem>, [<tag:items:forge:rawmeats>.asIIngredient(),<item:pamhc2foodcore:saltitem>]);
<recipetype:create:mixing>.addRecipe("phc_automix_cream", "none", <item:pamhc2foodcore:creamitem>, [], [<fluid:create:milk> * 125]);
//<recipetype:create:mixing>.addRecipe("phc_automix_cream_nofluid", "none", <item:pamhc2foodcore:creamitem>, [Milk250mb]);
<recipetype:create:mixing>.addRecipe("phc_automix_mashedpotato", "none", <item:pamhc2foodcore:mashedpotatoesitem>, [<item:minecraft:baked_potato>,<tag:items:forge:butter>.asIIngredient(),<item:pamhc2foodcore:saltitem>]);
<recipetype:create:mixing>.addRecipe("phc_automix_mashedpotato_garlic", "none", <item:pamhc2foodextended:garlicmashedpotatoesitem>, [<item:minecraft:baked_potato>,<tag:items:forge:butter>.asIIngredient(),<item:pamhc2foodcore:saltitem>,<item:pamhc2crops:garlicitem>]);
<recipetype:create:mixing>.addRecipe("phc_automix_mashed2garlicmashed", "none", <item:pamhc2foodextended:garlicmashedpotatoesitem>, [<item:pamhc2foodcore:mashedpotatoesitem>,<item:pamhc2crops:garlicitem>]);
craftingTable.addShapeless("phc_mixingbowl_mashed2garlicmashed", <item:pamhc2foodextended:garlicmashedpotatoesitem>, [<item:pamhc2foodcore:mixingbowlitem>.reuse(),<item:pamhc2foodcore:mashedpotatoesitem>, <item:pamhc2crops:garlicitem>]);
craftingTable.removeRecipe(<item:pamhc2foodextended:relishitem>);
craftingTable.addShapeless("phc_relish_fixed", <item:pamhc2foodextended:relishitem>, [<item:pamhc2foodcore:cuttingboarditem>.reuse(),<item:pamhc2crops:cucumberitem>]);
craftingTable.removeRecipe(<item:pamhc2foodcore:mayonaiseitem>);
craftingTable.addShapeless("phc_mayonnaise_fixed", <item:pamhc2foodcore:mayonaiseitem>, [<item:pamhc2foodcore:mixingbowlitem>.reuse(),<tag:items:forge:egg>.asIIngredient(),<tag:items:forge:cookingoil>.asIIngredient()]);
<recipetype:create:mixing>.addRecipe("phc_automix_mayonnaise", "none", <item:pamhc2foodcore:mayonaiseitem>, [<tag:items:forge:egg>.asIIngredient(),<tag:items:forge:cookingoil>.asIIngredient()]);
craftingTable.removeRecipe(<item:pamhc2foodextended:ketchupitem>);
craftingTable.addShapeless("phc_ketchup_fixed", <item:pamhc2foodextended:ketchupitem>, [<item:pamhc2foodcore:mixingbowlitem>.reuse(),<item:pamhc2crops:tomatoitem>,<tag:items:forge:sugar>.asIIngredient()]);
<recipetype:create:mixing>.addRecipe("phc_automix_ketchup", "none", <item:pamhc2foodextended:ketchupitem>, [<item:pamhc2crops:tomatoitem>,<tag:items:forge:sugar>.asIIngredient()]);
craftingTable.removeRecipe(<item:pamhc2foodextended:hotsauceitem>);
craftingTable.addShapeless("phc_hotsauce_fixed", <item:pamhc2foodextended:hotsauceitem>, [<item:pamhc2foodcore:mixingbowlitem>.reuse(),<item:pamhc2crops:chilipepperitem>,<tag:items:forge:cookingoil>.asIIngredient()]);
<recipetype:create:mixing>.addRecipe("phc_automix_hotsauce", "none", <item:pamhc2foodextended:hotsauceitem>, [<item:pamhc2crops:chilipepperitem>,<tag:items:forge:cookingoil>.asIIngredient()]);
	// Create's Millstone as an automated substitute for the Grinder
<recipetype:create:milling>.removeRecipe(<item:create:wheat_flour>);
<recipetype:create:milling>.addRecipe("phc_milling_flour_wheat", [<item:pamhc2foodcore:flouritem>,<item:pamhc2foodcore:flouritem> * 2 % 25,<item:minecraft:wheat_seeds> % 25], <item:minecraft:wheat>, DurationAverage);
<recipetype:create:milling>.addRecipe("phc_milling_flour_potato", [<item:pamhc2foodcore:flouritem>,<item:pamhc2foodcore:flouritem> * 2 % 25], <item:minecraft:potato>, DurationAverage);
<recipetype:create:milling>.addRecipe("phc_milling_butter_almond", [<item:pamhc2foodextended:almondbutteritem>,<item:pamhc2foodextended:almondbutteritem> % 5], <item:pamhc2trees:almonditem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_butter_cashew", [<item:pamhc2foodextended:cashewbutteritem>,<item:pamhc2foodextended:cashewbutteritem> % 5], <item:pamhc2trees:cashewitem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_butter_chestnut", [<item:pamhc2foodextended:chestnutbutteritem>,<item:pamhc2foodextended:chestnutbutteritem> % 5], <item:pamhc2trees:chestnutitem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_butter_hazelnut", [<item:pamhc2foodextended:hazelnutbutteritem>,<item:pamhc2foodextended:hazelnutbutteritem> % 5], <item:pamhc2trees:hazelnutitem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_butter_peanut", [<item:pamhc2foodextended:peanutbutteritem>,<item:pamhc2foodextended:peanutbutteritem> % 5], <item:pamhc2crops:peanutitem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_butter_pecan", [<item:pamhc2foodextended:pecanbutteritem>,<item:pamhc2foodextended:pecanbutteritem> % 5], <item:pamhc2trees:pecanitem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_butter_pistachio", [<item:pamhc2foodextended:pistachiobutteritem>,<item:pamhc2foodextended:pistachiobutteritem> % 5], <item:pamhc2trees:pistachioitem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_butter_walnut", [<item:pamhc2foodextended:walnutbutteritem>,<item:pamhc2foodextended:walnutbutteritem> % 5], <item:pamhc2trees:walnutitem>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_cocoapowder", [<item:pamhc2foodcore:cocoapowderitem>,<item:pamhc2foodcore:cocoapowderitem> % 10], <item:minecraft:cocoa_beans>, DurationEasy);
<recipetype:create:milling>.addRecipe("phc_milling_cornmeal", [<item:pamhc2foodextended:cornmealitem>,<item:pamhc2foodextended:cornmealitem> % 10], <item:pamhc2crops:cornitem>, DurationAverage);
<recipetype:create:milling>.addRecipe("phc_milling_pepper", [<item:pamhc2foodextended:blackpepperitem>,<item:pamhc2foodextended:blackpepperitem> % 10], <item:pamhc2trees:peppercornitem>, DurationAverage);
<recipetype:create:milling>.addRecipe("phc_milling_ground_beef", [<item:pamhc2foodcore:groundbeefitem>,<item:pamhc2foodcore:groundbeefitem> % 10], <tag:items:forge:rawmeats/rawbeef>.asIIngredient(), DurationHard);
<recipetype:create:milling>.addRecipe("phc_milling_ground_chicken", [<item:pamhc2foodcore:groundchickenitem>,<item:pamhc2foodcore:groundchickenitem> % 10], <tag:items:forge:rawmeats/rawchicken>.asIIngredient(), DurationHard);
<recipetype:create:milling>.addRecipe("phc_milling_ground_fish", [<item:pamhc2foodcore:groundfishitem>,<item:pamhc2foodcore:groundfishitem> % 10], <tag:items:minecraft:fishes>.asIIngredient(), DurationHard);
<recipetype:create:milling>.addRecipe("phc_milling_ground_mutton", [<item:pamhc2foodcore:groundmuttonitem>,<item:pamhc2foodcore:groundmuttonitem> % 10], <tag:items:forge:rawmeats/rawmutton>.asIIngredient(), DurationHard);
<recipetype:create:milling>.addRecipe("phc_milling_ground_pork", [<item:pamhc2foodcore:groundporkitem>,<item:pamhc2foodcore:groundporkitem> % 10], <tag:items:forge:rawmeats/rawpork>.asIIngredient(), DurationHard);
<recipetype:create:milling>.addRecipe("phc_milling_ground_rabbit", [<item:pamhc2foodcore:groundrabbititem>,<item:pamhc2foodcore:groundrabbititem> % 10], <tag:items:forge:rawmeats/rawrabbit>.asIIngredient(), DurationHard);
	// Create's Mechanical Press as an automated substitute for the Roller & Juicer, respectively
<recipetype:create:pressing>.addRecipe("phc_pressing_firmtofu", [<item:pamhc2foodextended:firmtofuitem>,<item:pamhc2foodextended:firmtofuitem> % 10], <item:pamhc2foodextended:silkentofuitem>, DurationEasy);
<recipetype:create:compacting>.addRecipe("phc_compact_mustard", "none", <item:pamhc2foodextended:mustarditem>, [<item:pamhc2crops:mustardseedsitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_soysauce", "none", <item:pamhc2foodextended:soysauceitem>, [<item:pamhc2crops:soybeanitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_cookingoil", "none", <item:pamhc2foodcore:cookingoilitem>, [<tag:items:forge:vegetables>.asIIngredient()]);
<recipetype:create:compacting>.addRecipe("phc_compact_sesameoil", "none", <item:pamhc2foodextended:sesameoilitem>, [<item:pamhc2crops:sesameseedsitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_mushroomoil", "none", <item:pamhc2foodextended:mushroomoilitem>, [<tag:items:forge:mushrooms>.asIIngredient()]);
	// YES, ADDING 49 SEPARATE JUICE RECIPES WAS HELL, EVEN WITH CTRL+H (Also some Juice recipes are broken/missing, so their fixes are here, too)
craftingTable.removeRecipe(<item:pamhc2foodcore:p8juiceitem>);
craftingTable.addShapeless("phc_p8_tomatomix", <item:pamhc2foodcore:p8juiceitem>, [<item:pamhc2foodcore:juiceritem>.reuse(),<tag:items:forge:vegetables>.asIIngredient(),<item:pamhc2crops:tomatoitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_p8", "none", <item:pamhc2foodcore:p8juiceitem>, [<tag:items:forge:vegetables>.asIIngredient(),<item:pamhc2crops:tomatoitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_apple", "none", <item:pamhc2foodcore:applejuiceitem>, [<item:minecraft:apple>,<item:minecraft:apple>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_apricot", "none", <item:pamhc2foodextended:apricotjuiceitem>, [<item:pamhc2trees:apricotitem>,<item:pamhc2trees:apricotitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_banana", "none", <item:pamhc2foodextended:bananajuiceitem>, [<item:pamhc2trees:bananaitem>,<item:pamhc2trees:bananaitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_blackberry", "none", <item:pamhc2foodextended:blackberryjuiceitem>, [<item:pamhc2crops:blackberryitem>,<item:pamhc2crops:blackberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_blueberry", "none", <item:pamhc2foodextended:blueberryjuiceitem>, [<item:pamhc2crops:blueberryitem>,<item:pamhc2crops:blueberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_breadfruit", "none", <item:pamhc2foodextended:breadfruitjuiceitem>, [<item:pamhc2trees:breadfruititem>,<item:pamhc2trees:breadfruititem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_cactusfruit", "none", <item:pamhc2foodextended:cactusfruitjuiceitem>, [<item:pamhc2crops:cactusfruititem>,<item:pamhc2crops:cactusfruititem>]);
craftingTable.removeRecipe(<item:pamhc2foodextended:candleberryjuiceitem>);
craftingTable.addShapeless("phc_juicer_juice_candleberry", <item:pamhc2foodextended:candleberryjuiceitem>, [<item:pamhc2foodcore:juiceritem>.reuse(),<item:pamhc2crops:candleberryitem>,<item:pamhc2crops:candleberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_candleberry", "none", <item:pamhc2foodextended:candleberryjuiceitem>, [<item:pamhc2crops:candleberryitem>,<item:pamhc2crops:candleberryitem>]);
craftingTable.addShapeless("phc_juicer_juice_cantaloupe", <item:pamhc2foodextended:cantaloupejuiceitem>, [<item:pamhc2foodcore:juiceritem>.reuse(),<item:pamhc2crops:cantaloupeitem>,<item:pamhc2crops:cantaloupeitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_cantaloupe", "none", <item:pamhc2foodextended:cantaloupejuiceitem>, [<item:pamhc2crops:cantaloupeitem>,<item:pamhc2crops:cantaloupeitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_cherry", "none", <item:pamhc2foodextended:cherryjuiceitem>, [<item:pamhc2trees:cherryitem>,<item:pamhc2trees:cherryitem>]);
craftingTable.addShapeless("phc_juicer_juice_cranberry", <item:pamhc2foodextended:cranberryjuiceitem>, [<item:pamhc2foodcore:juiceritem>.reuse(),<item:pamhc2crops:cranberryitem>,<item:pamhc2crops:cranberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_cranberry", "none", <item:pamhc2foodextended:cranberryjuiceitem>, [<item:pamhc2crops:cranberryitem>,<item:pamhc2crops:cranberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_date", "none", <item:pamhc2foodextended:datejuiceitem>, [<item:pamhc2trees:dateitem>,<item:pamhc2trees:dateitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_dragonfruit", "none", <item:pamhc2foodextended:dragonfruitjuiceitem>, [<item:pamhc2trees:dragonfruititem>,<item:pamhc2trees:dragonfruititem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_durian", "none", <item:pamhc2foodextended:durianjuiceitem>, [<item:pamhc2trees:durianitem>,<item:pamhc2trees:durianitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_elderberry", "none", <item:pamhc2foodextended:elderberryjuiceitem>, [<item:pamhc2crops:elderberryitem>,<item:pamhc2crops:elderberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_fig", "none", <item:pamhc2foodextended:figjuiceitem>, [<item:pamhc2trees:figitem>,<item:pamhc2trees:figitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_gooseberry", "none", <item:pamhc2foodextended:gooseberryjuiceitem>, [<item:pamhc2trees:gooseberryitem>,<item:pamhc2trees:gooseberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_grape", "none", <item:pamhc2foodextended:grapejuiceitem>, [<item:pamhc2crops:grapeitem>,<item:pamhc2crops:grapeitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_grape_green", "none", <item:pamhc2foodextended:greengrapejuiceitem>, [<item:pamhc2crops:greengrapeitem>,<item:pamhc2crops:greengrapeitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_grapefruit", "none", <item:pamhc2foodextended:grapefruitjuiceitem>, [<item:pamhc2trees:grapefruititem>,<item:pamhc2trees:grapefruititem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_guava", "none", <item:pamhc2foodextended:guavajuiceitem>, [<item:pamhc2trees:guavaitem>,<item:pamhc2trees:guavaitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_huckleberry", "none", <item:pamhc2foodextended:huckleberryjuiceitem>, [<item:pamhc2crops:huckleberryitem>,<item:pamhc2crops:huckleberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_jackfruit", "none", <item:pamhc2foodextended:jackfruitjuiceitem>, [<item:pamhc2trees:jackfruititem>,<item:pamhc2trees:jackfruititem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_juniperberry", "none", <item:pamhc2foodextended:juniperberryjuiceitem>, [<item:pamhc2crops:juniperberryitem>,<item:pamhc2crops:juniperberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_kiwi", "none", <item:pamhc2foodextended:kiwijuiceitem>, [<item:pamhc2crops:kiwiitem>,<item:pamhc2crops:kiwiitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_lemon", "none", <item:pamhc2foodextended:lemonjuiceitem>, [<item:pamhc2trees:lemonitem>,<item:pamhc2trees:lemonitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_lime", "none", <item:pamhc2foodextended:limejuiceitem>, [<item:pamhc2trees:limeitem>,<item:pamhc2trees:limeitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_lychee", "none", <item:pamhc2foodextended:lycheejuiceitem>, [<item:pamhc2trees:lycheeitem>,<item:pamhc2trees:lycheeitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_mango", "none", <item:pamhc2foodextended:mangojuiceitem>, [<item:pamhc2trees:mangoitem>,<item:pamhc2trees:mangoitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_melon", "none", <item:pamhc2foodcore:melonjuiceitem>, [<item:minecraft:melon_slice>,<item:minecraft:melon_slice>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_mulberry", "none", <item:pamhc2foodextended:mulberryjuiceitem>, [<item:pamhc2crops:mulberryitem>,<item:pamhc2crops:mulberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_orange", "none", <item:pamhc2foodextended:orangejuiceitem>, [<item:pamhc2trees:orangeitem>,<item:pamhc2trees:orangeitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_papaya", "none", <item:pamhc2foodextended:papayajuiceitem>, [<item:pamhc2trees:papayaitem>,<item:pamhc2trees:papayaitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_passionfruit", "none", <item:pamhc2foodextended:passionfruitjuiceitem>, [<item:pamhc2trees:passionfruititem>,<item:pamhc2trees:passionfruititem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_pawpaw", "none", <item:pamhc2foodextended:pawpawjuiceitem>, [<item:pamhc2trees:pawpawitem>,<item:pamhc2trees:pawpawitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_peach", "none", <item:pamhc2foodextended:peachjuiceitem>, [<item:pamhc2trees:peachitem>,<item:pamhc2trees:peachitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_pear", "none", <item:pamhc2foodextended:pearjuiceitem>, [<item:pamhc2trees:pearitem>,<item:pamhc2trees:pearitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_persimmon", "none", <item:pamhc2foodextended:persimmonjuiceitem>, [<item:pamhc2trees:persimmonitem>,<item:pamhc2trees:persimmonitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_pineapple", "none", <item:pamhc2foodextended:pineapplejuiceitem>, [<item:pamhc2crops:pineappleitem>,<item:pamhc2crops:pineappleitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_plum", "none", <item:pamhc2foodextended:plumjuiceitem>, [<item:pamhc2trees:plumitem>,<item:pamhc2trees:plumitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_pomegranate", "none", <item:pamhc2foodextended:pomegranatejuiceitem>, [<item:pamhc2trees:pomegranateitem>,<item:pamhc2trees:pomegranateitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_rambutan", "none", <item:pamhc2foodextended:rambutanjuiceitem>, [<item:pamhc2trees:rambutanitem>,<item:pamhc2trees:rambutanitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_raspberry", "none", <item:pamhc2foodextended:raspberryjuiceitem>, [<item:pamhc2crops:raspberryitem>,<item:pamhc2crops:raspberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_soursop", "none", <item:pamhc2foodextended:soursopjuiceitem>, [<item:pamhc2trees:soursopitem>,<item:pamhc2trees:soursopitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_starfruit", "none", <item:pamhc2foodextended:starfruitjuiceitem>, [<item:pamhc2trees:starfruititem>,<item:pamhc2trees:starfruititem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_strawberry", "none", <item:pamhc2foodextended:strawberryjuiceitem>, [<item:pamhc2crops:strawberryitem>,<item:pamhc2crops:strawberryitem>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_sweetberry", "none", <item:pamhc2foodcore:sweetberryjuiceitem>, [<item:minecraft:sweet_berries>,<item:minecraft:sweet_berries>]);
<recipetype:create:compacting>.addRecipe("phc_compact_juice_tamarind", "none", <item:pamhc2foodextended:tamarindjuiceitem>, [<item:pamhc2trees:tamarinditem>,<item:pamhc2trees:tamarinditem>]);
