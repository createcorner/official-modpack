// DOCUMENTATION:	https://docs.blamejared.com/1.16/en/index/

// === Item Tags === //

	// Inspiration Dye Removal, because the crafting dye in cauldrons mechanic is disabled
<tag:items:forge:dyes/black>.remove(<item:inspirations:black_dyed_bottle>);
<tag:items:forge:dyes/blue>.remove(<item:inspirations:blue_dyed_bottle>);
<tag:items:forge:dyes/brown>.remove(<item:inspirations:brown_dyed_bottle>);
<tag:items:forge:dyes/cyan>.remove(<item:inspirations:cyan_dyed_bottle>);
<tag:items:forge:dyes/gray>.remove(<item:inspirations:gray_dyed_bottle>);
<tag:items:forge:dyes/green>.remove(<item:inspirations:green_dyed_bottle>);
<tag:items:forge:dyes/light_blue>.remove(<item:inspirations:light_blue_dyed_bottle>);
<tag:items:forge:dyes/light_gray>.remove(<item:inspirations:light_gray_dyed_bottle>);
<tag:items:forge:dyes/lime>.remove(<item:inspirations:lime_dyed_bottle>);
<tag:items:forge:dyes/magenta>.remove(<item:inspirations:magenta_dyed_bottle>);
<tag:items:forge:dyes/orange>.remove(<item:inspirations:orange_dyed_bottle>);
<tag:items:forge:dyes/pink>.remove(<item:inspirations:pink_dyed_bottle>);
<tag:items:forge:dyes/purple>.remove(<item:inspirations:purple_dyed_bottle>);
<tag:items:forge:dyes/red>.remove(<item:inspirations:red_dyed_bottle>);
<tag:items:forge:dyes/white>.remove(<item:inspirations:white_dyed_bottle>);
<tag:items:forge:dyes/yellow>.remove(<item:inspirations:yellow_dyed_bottle>);
	// Extended Caves disabled stone removal
<tag:items:forge:stone>.remove(<item:extcaves:dirtstone>);
<tag:items:forge:stone>.remove(<item:extcaves:lavastone>);
<tag:items:forge:stone>.remove(<item:extcaves:oldstone>);
<tag:items:forge:stone>.remove(<item:extcaves:sedimentstone>);
<tag:items:forge:cobblestone>.remove(<item:extcaves:dirtstone_cobble>);
	// I Like Wood crafting table Compat
<tag:items:ilikewood:crafting_tables>.remove(<item:ilikewood:oak_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:minecraft:crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:dragon_tree_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:end_lotus_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:helix_tree_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:jellyshroom_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:lacugrove_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:mossy_glowshroom_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:pythadendron_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:tenanea_crafting_table>);
<tag:items:ilikewood:crafting_tables>.add(<item:betterendforge:umbrella_tree_crafting_table>);
	// Assorted Compat/Removals
<tag:items:create:crushed_ores>.add(<item:contenttweaker:crushed_netherite>);
<tag:items:create:crushed_ores>.add(<item:contenttweaker:crushed_terminite>);
<tag:items:create:crushed_ores>.add(<item:contenttweaker:crushed_aeternium>);
var AnyCobble = <tag:items:forge:cobblestone>.asIIngredient();
<tag:items:forge:hides>.add(<item:minecraft:leather>);
<tag:items:forge:hides>.add(<item:minecraft:rabbit_hide>);
<tag:items:forge:crushable_quartz>.add(<item:minecraft:chiseled_quartz_block>);
<tag:items:forge:crushable_quartz>.add(<item:minecraft:quartz_block>);
<tag:items:forge:crushable_quartz>.add(<item:minecraft:quartz_bricks>);
<tag:items:forge:crushable_quartz>.add(<item:minecraft:quartz_pillar>);
<tag:items:forge:crushable_quartz>.add(<item:minecraft:smooth_quartz>);
	// Create Machine Duration Constants // #sec ÷ 0.05
var DurationInstant = 20; //1s
var DurationEasiest = 40; //2s
var DurationEasy = 100;	//5s
var DurationAverage = 200; //10s
var DurationHard = 600; //30s
var DurationHardest = 1200; //1m
var DurationExtreme = 2400; //2m


// === Vanilla Recipe Tweaks === //

craftingTable.addShapeless("diy_nametag", <item:minecraft:name_tag>, [<item:minecraft:paper>,<item:minecraft:string>,<tag:items:forge:dyes/black>.asIIngredient(),<item:minecraft:feather>]);
craftingTable.addShaped("diy_bell", <item:minecraft:bell>, [
	[<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>],
	[<item:minecraft:gold_ingot>,<item:minecraft:gold_ingot>,<item:minecraft:gold_ingot>],
	[<item:minecraft:gold_ingot>,<item:minecraft:gold_nugget>,<item:minecraft:gold_ingot>]
]);
craftingTable.addShaped("diy_saddle", <item:minecraft:saddle>, [
	[<item:minecraft:leather>,<item:minecraft:leather>,<item:minecraft:leather>],
	[<item:minecraft:leather>,<item:minecraft:air>,<item:minecraft:leather>],
	[<item:minecraft:tripwire_hook>,<item:minecraft:air>,<item:minecraft:tripwire_hook>]
]);
craftingTable.addShapeless("diy_iceblock", <item:minecraft:ice>, [
	<item:minecraft:water_bucket>.transformReplace(<item:minecraft:bucket>),<item:minecraft:snowball>,<item:minecraft:snowball>,
	<item:minecraft:snowball>,<item:minecraft:snowball>,<item:minecraft:snowball>,
	<item:minecraft:snowball>,<item:minecraft:snowball>,<item:minecraft:snowball>
]);
/*craftingTable.addShapeless("diy_phantom_membrane", <item:minecraft:phantom_membrane>, [
	<tag:items:forge:feathers>.asIIngredient(),<tag:items:forge:feathers>.asIIngredient(),<tag:items:forge:feathers>.asIIngredient(),
	<tag:items:forge:feathers>.asIIngredient(),<tag:items:forge:hides>.asIIngredient(),<tag:items:forge:slimeballs>.asIIngredient(),
	<item:betterendforge:ender_dust>,<item:betterendforge:ender_dust>,<item:betterendforge:ender_dust>
]);*/

craftingTable.removeRecipe(<item:minecraft:stone_axe>);
craftingTable.addShapedMirrored("any_cobblestone_axe", <item:minecraft:stone_axe>, [
	[AnyCobble,AnyCobble],
	[AnyCobble,<item:minecraft:stick>],
	[<item:minecraft:air>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:minecraft:stone_hoe>);
craftingTable.addShapedMirrored("any_cobblestone_hoe", <item:minecraft:stone_hoe>, [
	[AnyCobble,AnyCobble],
	[<item:minecraft:air>,<item:minecraft:stick>],
	[<item:minecraft:air>,<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:minecraft:stone_pickaxe>);
craftingTable.addShaped("any_cobblestone_pickaxe", <item:minecraft:stone_pickaxe>, [
	[AnyCobble,AnyCobble,AnyCobble],
	[<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>],
	[<item:minecraft:air>,<item:minecraft:stick>,<item:minecraft:air>]
]);
craftingTable.removeRecipe(<item:minecraft:stone_shovel>);
craftingTable.addShaped("any_cobblestone_shovel", <item:minecraft:stone_shovel>, [
	[AnyCobble],
	[<item:minecraft:stick>],
	[<item:minecraft:stick>]
]);
craftingTable.removeRecipe(<item:minecraft:stone_sword>);
craftingTable.addShaped("any_cobblestone_sword", <item:minecraft:stone_sword>, [
	[AnyCobble],
	[AnyCobble],
	[<item:minecraft:stick>]
]);


// === Create Tweaks & Compat (+Content Tweaker) === //

craftingTable.removeRecipe(<item:create:dough>);
craftingTable.removeByName("create:crafting/appliances/slime_ball");
craftingTable.addShapeless("phc_slimeball_create-compat", <item:minecraft:slime_ball>, [<item:pamhc2foodcore:doughitem>, <tag:items:forge:dyes/lime>.asIIngredient()]);
//<recipetype:create:mixing>.addRecipe("phc_automix_dough_nofluid", "none", <item:pamhc2foodcore:doughitem> * 2, [<tag:items:forge:flour>.asIIngredient(),<item:pamhc2foodcore:freshwateritem>,<item:pamhc2foodcore:saltitem>]);
<recipetype:create:mixing>.addRecipe("phc_automix_dough", "none", <item:pamhc2foodcore:doughitem> * 2, [<tag:items:forge:flour>.asIIngredient(),<item:pamhc2foodcore:saltitem>], [<fluid:minecraft:water> * 125]);

<recipetype:create:compacting>.addRecipe("compact_diy_podzol", "none", <item:minecraft:podzol>, [<item:minecraft:dirt>,<item:minecraft:spruce_leaves>], [<fluid:minecraft:water> * 250]);
<recipetype:create:compacting>.addRecipe("compact_diy_mycelium", "none", <item:minecraft:mycelium>, [<item:minecraft:dirt>,<tag:items:forge:mushrooms>.asIIngredient()], [<fluid:minecraft:water> * 250]);

craftingTable.addShapeless("ingot2nugget_netherite", <item:contenttweaker:netherite_nugget> * 9, [<item:minecraft:netherite_ingot>]);
craftingTable.addShapeless("ingot2nugget_terminite", <item:contenttweaker:terminite_nugget> * 9, [<item:betterendforge:terminite_ingot>]);
craftingTable.addShapeless("ingot2nugget_aeternium", <item:contenttweaker:aeternium_nugget> * 9, [<item:betterendforge:aeternium_ingot>]);
craftingTable.addShapeless("nugget2ingot_netherite", <item:minecraft:netherite_ingot>,[<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>,<item:contenttweaker:netherite_nugget>]);
craftingTable.addShapeless("nugget2ingot_terminite", <item:betterendforge:terminite_ingot>,[<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>,<item:contenttweaker:terminite_nugget>]);
craftingTable.addShapeless("nugget2ingot_aeternium", <item:betterendforge:aeternium_ingot>,[<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>,<item:contenttweaker:aeternium_nugget>]);
furnace.addRecipe("smelting_crushednetherite", <item:minecraft:netherite_ingot>, <item:contenttweaker:crushed_netherite>, 0.1, DurationAverage);
furnace.addRecipe("smelting_crushednetherite", <item:betterendforge:terminite_ingot>, <item:contenttweaker:crushed_terminite>, 0.1, DurationAverage);
furnace.addRecipe("smelting_crushednetherite", <item:betterendforge:aeternium_ingot>, <item:contenttweaker:crushed_aeternium>, 0.15, 250); //12.5s
blastFurnace.addRecipe("blasting_crushednetherite", <item:minecraft:netherite_ingot>, <item:contenttweaker:crushed_netherite>, 0.1, DurationHard);
blastFurnace.addRecipe("blasting_crushednetherite", <item:betterendforge:terminite_ingot>, <item:contenttweaker:crushed_terminite>, 0.1, DurationHard);
blastFurnace.addRecipe("blasting_crushednetherite", <item:betterendforge:aeternium_ingot>, <item:contenttweaker:crushed_aeternium>, 0.15, 800); //40s

<recipetype:create:crushing>.addRecipe("crushing_ironblock", [<item:create:crushed_iron_ore> * 5], <item:minecraft:iron_block>);
<recipetype:create:crushing>.addRecipe("crushing_goldblock", [<item:create:crushed_gold_ore> * 5], <item:minecraft:gold_block>);
<recipetype:create:crushing>.addRecipe("crushing_netheriteblock", [<item:contenttweaker:crushed_netherite> * 5], <item:minecraft:netherite_block>);
<recipetype:create:crushing>.addRecipe("crushing_terminiteblock", [<item:contenttweaker:crushed_terminite> * 5], <item:betterendforge:terminite_block>);
<recipetype:create:crushing>.addRecipe("crushing_aeterniumblock", [<item:contenttweaker:crushed_aeternium> * 5], <item:betterendforge:aeternium_block>);

<recipetype:create:crushing>.removeRecipe(<item:create:cinder_flour>);
<recipetype:create:crushing>.addRecipe("crushing_netherrack_sulphur", [<item:create:cinder_flour>,<item:create:cinder_flour> % 50,<item:betterendforge:crystalline_sulphur> % 15], <item:minecraft:netherrack>);
<recipetype:create:crushing>.removeRecipe(<item:create:crushed_gold_ore>);
<recipetype:create:crushing>.addRecipe("milling_goldore_stoneonly", [<item:create:crushed_gold_ore>], <item:minecraft:gold_ore>);
<recipetype:create:crushing>.addRecipe("crushing_goldore_stoneonly", [<item:create:crushed_gold_ore>,<item:create:crushed_gold_ore> * 2 % 30,<item:minecraft:cobblestone> % 12], <item:minecraft:gold_ore>);
<recipetype:create:crushing>.addRecipe("milling_nethergoldore", [<item:minecraft:gold_nugget> * 6], <item:minecraft:nether_gold_ore>);
<recipetype:create:crushing>.addRecipe("crushing_nethergoldore", [<item:create:crushed_gold_ore>,<item:create:cinder_flour> % 50,<item:betterendforge:crystalline_sulphur> % 20], <item:minecraft:nether_gold_ore>);
<recipetype:create:crushing>.addRecipe("crushing_quartzblocks", [<item:minecraft:quartz> * 3,<item:minecraft:quartz> % 50], <tag:items:forge:crushable_quartz>.asIIngredient());

<recipetype:create:splashing>.addRecipe("bulkwash_netherite", [<item:contenttweaker:netherite_nugget> * 10,<item:contenttweaker:netherite_nugget> * 5 % 50], <item:contenttweaker:crushed_netherite>);
<recipetype:create:splashing>.addRecipe("bulkwash_terminite", [<item:contenttweaker:terminite_nugget> * 10,<item:contenttweaker:terminite_nugget> * 5 % 50], <item:contenttweaker:crushed_terminite>);
<recipetype:create:splashing>.addRecipe("bulkwash_aeternium", [<item:contenttweaker:aeternium_nugget> * 10,<item:contenttweaker:aeternium_nugget> * 5 % 50], <item:contenttweaker:crushed_aeternium>);

<recipetype:create:crushing>.addRecipe("crushing_redalloyingot", [<item:minecraft:redstone> * 3,<item:minecraft:redstone> % 50], <item:morered:red_alloy_ingot>);
<recipetype:create:mixing>.addRecipe("mixeralloying_redalloyingot", "heated", <item:morered:red_alloy_ingot>, [<item:minecraft:iron_ingot>,<item:minecraft:redstone>,<item:minecraft:redstone>,<item:minecraft:redstone>,<item:minecraft:redstone>]);
<recipetype:create:mixing>.addRecipe("mixeralloying_terminite", "heated", <item:betterendforge:terminite_ingot>, [<item:minecraft:iron_ingot>,<item:betterendforge:ender_dust>]);
<recipetype:create:mixing>.addRecipe("mixeralloying_terminite_crushed", "heated", <item:contenttweaker:crushed_terminite>, [<item:create:crushed_iron_ore>,<item:betterendforge:ender_dust>]);
<recipetype:create:mixing>.addRecipe("mixeralloying_aeternium", "superheated", <item:betterendforge:aeternium_ingot> * 2, [<item:betterendforge:terminite_ingot>,<item:minecraft:netherite_ingot>,<item:betterendforge:endstone_dust>]);
<recipetype:create:mixing>.addRecipe("mixeralloying_aeternium_crushed", "superheated", <item:contenttweaker:crushed_aeternium> * 2, [<item:contenttweaker:crushed_terminite>,<item:contenttweaker:crushed_netherite>,<item:betterendforge:endstone_dust>]);

	// Mixing up Concrete Powder. Why is this not already part of Create?
<recipetype:create:mixing>.addRecipe("mixing_concrete_black", "none", <item:minecraft:black_concrete_powder>, [<tag:items:forge:dyes/black>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_blue", "none", <item:minecraft:blue_concrete_powder> * 8, [<tag:items:forge:dyes/blue>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_brown", "none", <item:minecraft:brown_concrete_powder> * 8, [<tag:items:forge:dyes/brown>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_cyan", "none", <item:minecraft:cyan_concrete_powder> * 8, [<tag:items:forge:dyes/cyan>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_gray", "none", <item:minecraft:gray_concrete_powder> * 8, [<tag:items:forge:dyes/gray>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_green", "none", <item:minecraft:green_concrete_powder> * 8, [<tag:items:forge:dyes/green>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_light_blue", "none", <item:minecraft:light_blue_concrete_powder> * 8, [<tag:items:forge:dyes/light_blue>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_light_gray", "none", <item:minecraft:light_gray_concrete_powder> * 8, [<tag:items:forge:dyes/light_gray>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_lime", "none", <item:minecraft:lime_concrete_powder> * 8, [<tag:items:forge:dyes/lime>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_magenta", "none", <item:minecraft:magenta_concrete_powder> * 8, [<tag:items:forge:dyes/magenta>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_orange", "none", <item:minecraft:orange_concrete_powder> * 8, [<tag:items:forge:dyes/orange>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_pink", "none", <item:minecraft:pink_concrete_powder> * 8, [<tag:items:forge:dyes/pink>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_purple", "none", <item:minecraft:purple_concrete_powder> * 8, [<tag:items:forge:dyes/purple>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_red", "none", <item:minecraft:red_concrete_powder> * 8, [<tag:items:forge:dyes/red>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_white", "none", <item:minecraft:white_concrete_powder> * 8, [<tag:items:forge:dyes/white>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);
<recipetype:create:mixing>.addRecipe("mixing_concrete_yellow", "none", <item:minecraft:yellow_concrete_powder> * 8, [<tag:items:forge:dyes/yellow>.asIIngredient(),
	<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),<tag:items:forge:sand>.asIIngredient(),
	<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient(),<tag:items:forge:gravel>.asIIngredient()
]);


// === Better End Adjustments === //

<recipetype:betterendforge:alloying>.removeAll();
<recipetype:betterendforge:anvil_smithing>.removeAll();
craftingTable.removeRecipe(<item:betterendforge:end_stone_smelter>);
craftingTable.removeByRegex(".*betterendforge.*hammer");

smithing.addRecipe("aeternium_axe_head", <item:betterendforge:aeternium_axe_head>, <item:betterendforge:terminite_axe>,<item:betterendforge:aeternium_ingot>);
smithing.addRecipe("aeternium_hoe_head", <item:betterendforge:aeternium_hoe_head>, <item:betterendforge:terminite_hoe>,<item:betterendforge:aeternium_ingot>);
smithing.addRecipe("aeternium_pickaxe_head", <item:betterendforge:aeternium_pickaxe_head>, <item:betterendforge:terminite_pickaxe>,<item:betterendforge:aeternium_ingot>);
smithing.addRecipe("aeternium_shovel_head", <item:betterendforge:aeternium_shovel_head>, <item:betterendforge:terminite_shovel>,<item:betterendforge:aeternium_ingot>);
smithing.addRecipe("aeternium_sword_blade", <item:betterendforge:aeternium_sword_blade>, <item:betterendforge:terminite_sword>,<item:betterendforge:aeternium_ingot>);

craftingTable.removeRecipe(<item:betterendforge:ender_block>);
craftingTable.addShaped("be_enderblock_expensive", <item:betterendforge:ender_block>, [[<item:minecraft:ender_pearl>,<item:minecraft:ender_pearl>,<item:minecraft:ender_pearl>],[<item:minecraft:ender_pearl>,<item:minecraft:ender_pearl>,<item:minecraft:ender_pearl>],[<item:minecraft:ender_pearl>,<item:minecraft:ender_pearl>,<item:minecraft:ender_pearl>]]);
craftingTable.removeByName("minecraft:ender_pearl_from_ender_block");
craftingTable.addShapeless("be_enderblock2pearl", <item:minecraft:ender_pearl> * 9, [<item:betterendforge:ender_block>]);
<recipetype:create:crushing>.addRecipe("be_enderblock_crushed", [<item:betterendforge:ender_shard> * 5, <item:betterendforge:ender_shard> * 2 % 15, <item:betterendforge:ender_dust> % 30], <item:betterendforge:ender_block>, DurationHard);
<recipetype:create:milling>.addRecipe("be_milling_enderpearl", [<item:betterendforge:ender_dust>,<item:betterendforge:ender_dust> % 30], <item:minecraft:ender_pearl>);
<recipetype:create:milling>.addRecipe("be_milling_endershard", [<item:betterendforge:ender_dust>,<item:betterendforge:ender_dust> % 10], <item:betterendforge:ender_shard>);
<recipetype:create:milling>.addRecipe("be_milling_endstone", [<item:betterendforge:endstone_dust>], <tag:items:forge:end_stones>.asIIngredient());
<recipetype:create:crushing>.addRecipe("be_crushing_endstone", [<item:betterendforge:endstone_dust>,<item:betterendforge:endstone_dust> % 35], <tag:items:forge:end_stones>.asIIngredient());


// === Adorn Tweaks & Dupe Cleanup === //

craftingTable.removeByRegex(".*adorn.*crate.*");
craftingTable.removeRecipe(<item:adorn:picket_fence>);
craftingTable.addShapeless("adorn_picketfence_expensive", <item:adorn:picket_fence> * 3, [<tag:items:forge:dyes/white>.asIIngredient(),<tag:items:forge:fences/wooden>.asIIngredient(),<tag:items:forge:fences/wooden>.asIIngredient(),<tag:items:forge:fences/wooden>.asIIngredient()]);


// === Inspirations Fixes === //

craftingTable.removeRecipe(<item:inspirations:collector>);
craftingTable.removeRecipe(<item:inspirations:pipe>);
craftingTable.removeRecipe(<item:inspirations:barometer>);
craftingTable.removeRecipe(<item:inspirations:photometer>);
craftingTable.removeRecipe(<item:inspirations:north_compass>);
craftingTable.removeRecipe(<item:inspirations:potato_soup>);
craftingTable.removeByRegex(".*waypoint_compass.*");
craftingTable.removeByRegex(".*enlightened.*");

<recipetype:create:filling>.removeRecipe(<item:inspirations:beetroot_soup_bucket>);
<recipetype:create:filling>.removeRecipe(<item:inspirations:honey_bucket>);
<recipetype:create:filling>.removeRecipe(<item:inspirations:mushroom_stew_bucket>);
<recipetype:create:filling>.removeRecipe(<item:inspirations:potato_soup_bucket>);
<recipetype:create:filling>.removeRecipe(<item:inspirations:rabbit_stew_bucket>);
<recipetype:create:emptying>.removeRecipe(<fluid:inspirations:beetroot_soup> * 1000);
<recipetype:create:emptying>.removeRecipe(<fluid:inspirations:honey> * 1000);
<recipetype:create:emptying>.removeRecipe(<fluid:inspirations:milk> * 1000);
<recipetype:create:emptying>.removeRecipe(<fluid:inspirations:mushroom_stew> * 1000);
<recipetype:create:emptying>.removeRecipe(<fluid:inspirations:potato_soup> * 1000);
<recipetype:create:emptying>.removeRecipe(<fluid:inspirations:rabbit_stew> * 1000);


// === Improved Stations & I Like Wood Removals (+Minor Compat) === //

craftingTable.removeRecipe(<item:improved-stations:blast_furnace_slab>);
craftingTable.removeRecipe(<item:improved-stations:crafting_table_slab>);
craftingTable.removeRecipe(<item:improved-stations:furnace_slab>);
craftingTable.removeRecipe(<item:improved-stations:jukebox_slab>);
craftingTable.removeRecipe(<item:improved-stations:loom_slab>);
craftingTable.removeRecipe(<item:improved-stations:smoker_slab>);

craftingTable.removeRecipe(<item:improved-stations:crafting_station>);
craftingTable.addShapeless("ilw_craftingstation", <item:improved-stations:crafting_station>, [<tag:items:ilikewood:crafting_tables>.asIIngredient()]);
craftingTable.removeRecipe(<item:ilikewood:oak_crafting_table>);

craftingTable.removeByRegex(".*ilikewood.*axe.*");
craftingTable.removeByRegex(".*ilikewood.*bow.*");
craftingTable.removeByRegex(".*ilikewood.*hoe.*");
craftingTable.removeByRegex(".*ilikewood.*shovel.*");
craftingTable.removeByRegex(".*ilikewood.*sword.*");

craftingTable.removeRecipe(<item:ilikewood:acacia_chest>);
craftingTable.removeRecipe(<item:ilikewood:acacia_ladder>);
craftingTable.removeRecipe(<item:ilikewood:birch_chest>);
craftingTable.removeRecipe(<item:ilikewood:birch_ladder>);
craftingTable.removeRecipe(<item:ilikewood:crimson_chest>);
craftingTable.removeRecipe(<item:ilikewood:crimson_ladder>);
craftingTable.removeRecipe(<item:ilikewood:dark_oak_chest>);
craftingTable.removeRecipe(<item:ilikewood:dark_oak_ladder>);
craftingTable.removeRecipe(<item:ilikewood:jungle_chest>);
craftingTable.removeRecipe(<item:ilikewood:jungle_ladder>);
craftingTable.removeRecipe(<item:ilikewood:oak_chest>);
craftingTable.removeRecipe(<item:ilikewood:oak_ladder>);
craftingTable.removeRecipe(<item:ilikewood:spruce_chest>);
craftingTable.removeRecipe(<item:ilikewood:spruce_ladder>);
craftingTable.removeRecipe(<item:ilikewood:warped_chest>);
craftingTable.removeRecipe(<item:ilikewood:warped_ladder>);

stoneCutter.addRecipe("ilw_plank2panel_acacia", <item:ilikewood:acacia_panels>, <item:minecraft:acacia_planks>);
stoneCutter.addRecipe("ilw_panel2plank_acacia", <item:minecraft:acacia_planks>, <item:ilikewood:acacia_panels>);
stoneCutter.addRecipe("ilw_plank2panel_birch", <item:ilikewood:birch_panels>, <item:minecraft:birch_planks>);
stoneCutter.addRecipe("ilw_panel2plank_birch", <item:minecraft:birch_planks>, <item:ilikewood:birch_panels>);
stoneCutter.addRecipe("ilw_plank2panel_crimson", <item:ilikewood:crimson_panels>, <item:minecraft:crimson_planks>);
stoneCutter.addRecipe("ilw_panel2plank_crimson", <item:minecraft:crimson_planks>, <item:ilikewood:crimson_panels>);
stoneCutter.addRecipe("ilw_plank2panel_dark_oak", <item:ilikewood:dark_oak_panels>, <item:minecraft:dark_oak_planks>);
stoneCutter.addRecipe("ilw_panel2plank_dark_oak", <item:minecraft:dark_oak_planks>, <item:ilikewood:dark_oak_panels>);
stoneCutter.addRecipe("ilw_plank2panel_jungle", <item:ilikewood:jungle_panels>, <item:minecraft:jungle_planks>);
stoneCutter.addRecipe("ilw_panel2plank_jungle", <item:minecraft:jungle_planks>, <item:ilikewood:jungle_panels>);
stoneCutter.addRecipe("ilw_plank2panel_oak", <item:ilikewood:oak_panels>, <item:minecraft:oak_planks>);
stoneCutter.addRecipe("ilw_panel2plank_oak", <item:minecraft:oak_planks>, <item:ilikewood:oak_panels>);
stoneCutter.addRecipe("ilw_plank2panel_spruce", <item:ilikewood:spruce_panels>, <item:minecraft:spruce_planks>);
stoneCutter.addRecipe("ilw_panel2plank_spruce", <item:minecraft:spruce_planks>, <item:ilikewood:spruce_panels>);
stoneCutter.addRecipe("ilw_plank2panel_warped", <item:ilikewood:warped_panels>, <item:minecraft:warped_planks>);
stoneCutter.addRecipe("ilw_panel2plank_warped", <item:minecraft:warped_planks>, <item:ilikewood:warped_panels>);

stoneCutter.addRecipe("ilw_plank2panel_cherry", <item:ilikewood:cherry_panels>, <item:biomesoplenty:cherry_planks>);
stoneCutter.addRecipe("ilw_panel2plank_cherry", <item:biomesoplenty:cherry_planks>, <item:ilikewood:cherry_panels>);
stoneCutter.addRecipe("ilw_plank2panel_dead", <item:ilikewood:dead_panels>, <item:biomesoplenty:dead_planks>);
stoneCutter.addRecipe("ilw_panel2plank_dead", <item:biomesoplenty:dead_planks>, <item:ilikewood:dead_panels>);
stoneCutter.addRecipe("ilw_plank2panel_fir", <item:ilikewood:fir_panels>, <item:biomesoplenty:fir_planks>);
stoneCutter.addRecipe("ilw_panel2plank_fir", <item:biomesoplenty:fir_planks>, <item:ilikewood:fir_panels>);
stoneCutter.addRecipe("ilw_plank2panel_hellbark", <item:ilikewood:hellbark_panels>, <item:biomesoplenty:hellbark_planks>);
stoneCutter.addRecipe("ilw_panel2plank_hellbark", <item:biomesoplenty:hellbark_planks>, <item:ilikewood:hellbark_panels>);
stoneCutter.addRecipe("ilw_plank2panel_jacaranda", <item:ilikewood:jacaranda_panels>, <item:biomesoplenty:jacaranda_planks>);
stoneCutter.addRecipe("ilw_panel2plank_jacaranda", <item:biomesoplenty:jacaranda_planks>, <item:ilikewood:jacaranda_panels>);
stoneCutter.addRecipe("ilw_plank2panel_magic", <item:ilikewood:magic_panels>, <item:biomesoplenty:magic_planks>);
stoneCutter.addRecipe("ilw_panel2plank_magic", <item:biomesoplenty:magic_planks>, <item:ilikewood:magic_panels>);
stoneCutter.addRecipe("ilw_plank2panel_mahogany", <item:ilikewood:mahogany_panels>, <item:biomesoplenty:mahogany_planks>);
stoneCutter.addRecipe("ilw_panel2plank_mahogany", <item:biomesoplenty:mahogany_planks>, <item:ilikewood:mahogany_panels>);
stoneCutter.addRecipe("ilw_plank2panel_palm", <item:ilikewood:palm_panels>, <item:biomesoplenty:palm_planks>);
stoneCutter.addRecipe("ilw_panel2plank_palm", <item:biomesoplenty:palm_planks>, <item:ilikewood:palm_panels>);
stoneCutter.addRecipe("ilw_plank2panel_redwood", <item:ilikewood:redwood_panels>, <item:biomesoplenty:redwood_planks>);
stoneCutter.addRecipe("ilw_panel2plank_redwood", <item:biomesoplenty:redwood_planks>, <item:ilikewood:redwood_panels>);
stoneCutter.addRecipe("ilw_plank2panel_umbran", <item:ilikewood:umbran_panels>, <item:biomesoplenty:umbran_planks>);
stoneCutter.addRecipe("ilw_panel2plank_umbran", <item:biomesoplenty:umbran_planks>, <item:ilikewood:umbran_panels>);
stoneCutter.addRecipe("ilw_plank2panel_willow", <item:ilikewood:willow_panels>, <item:biomesoplenty:willow_planks>);
stoneCutter.addRecipe("ilw_panel2plank_willow", <item:biomesoplenty:willow_planks>, <item:ilikewood:willow_panels>);


// === Extended Caves Additions (& Removals) === //

craftingTable.addShaped("extc_snowbrick", <item:extcaves:bricks_snow> * 4, [[<item:minecraft:snow_block>,<item:minecraft:snow_block>],[<item:minecraft:snow_block>,<item:minecraft:snow_block>]]);
craftingTable.addShaped("extc_snowbrick_slab", <item:extcaves:bricks_snow_slab> * 6, [[<item:extcaves:bricks_snow>,<item:extcaves:bricks_snow>,<item:extcaves:bricks_snow>]]);
craftingTable.addShapedMirrored("extc_snowbrick_stairs", <item:extcaves:bricks_snow_stairs> * 4, [
	[<item:extcaves:bricks_snow>,<item:minecraft:air>,<item:minecraft:air>],
	[<item:extcaves:bricks_snow>,<item:extcaves:bricks_snow>,<item:minecraft:air>],
	[<item:extcaves:bricks_snow>,<item:extcaves:bricks_snow>,<item:extcaves:bricks_snow>]
]);

craftingTable.addShaped("extc_icebrick", <item:extcaves:bricks_ice> * 4, [[<item:minecraft:packed_ice>,<item:minecraft:packed_ice>],[<item:minecraft:packed_ice>,<item:minecraft:packed_ice>]]);
craftingTable.addShaped("extc_icebrick_slab", <item:extcaves:bricks_ice_slab> * 6, [[<item:extcaves:bricks_ice>,<item:extcaves:bricks_ice>,<item:extcaves:bricks_ice>]]);
craftingTable.addShapedMirrored("extc_icebrick_stairs", <item:extcaves:bricks_ice_stairs> * 4, [
	[<item:extcaves:bricks_ice>,<item:minecraft:air>,<item:minecraft:air>],
	[<item:extcaves:bricks_ice>,<item:extcaves:bricks_ice>,<item:minecraft:air>],
	[<item:extcaves:bricks_ice>,<item:extcaves:bricks_ice>,<item:extcaves:bricks_ice>]
]);

craftingTable.removeByName("extcaves:pebble_stone");
stoneCutter.addRecipe("extc_stone2pebble", <item:extcaves:pebble_stone> * 9, <item:minecraft:stone>);
stoneCutter.addRecipe("extc_andesite2pebble", <item:extcaves:pebble_andesite> * 9, <item:minecraft:andesite>);
stoneCutter.addRecipe("extc_diorite2pebble", <item:extcaves:pebble_diorite> * 9, <item:minecraft:diorite>);
stoneCutter.addRecipe("extc_granite2pebble", <item:extcaves:pebble_granite> * 9, <item:minecraft:granite>);
craftingTable.addShaped("extc_pebble2andesite", <item:minecraft:andesite>, [[<item:extcaves:pebble_andesite>,<item:extcaves:pebble_andesite>,<item:extcaves:pebble_andesite>],[<item:extcaves:pebble_andesite>,<item:extcaves:pebble_andesite>,<item:extcaves:pebble_andesite>],[<item:extcaves:pebble_andesite>,<item:extcaves:pebble_andesite>,<item:extcaves:pebble_andesite>]]);
craftingTable.addShaped("extc_pebble2diorite", <item:minecraft:diorite>, [[<item:extcaves:pebble_diorite>,<item:extcaves:pebble_diorite>,<item:extcaves:pebble_diorite>],[<item:extcaves:pebble_diorite>,<item:extcaves:pebble_diorite>,<item:extcaves:pebble_diorite>],[<item:extcaves:pebble_diorite>,<item:extcaves:pebble_diorite>,<item:extcaves:pebble_diorite>]]);
craftingTable.addShaped("extc_pebble2granite", <item:minecraft:granite>, [[<item:extcaves:pebble_granite>,<item:extcaves:pebble_granite>,<item:extcaves:pebble_granite>],[<item:extcaves:pebble_granite>,<item:extcaves:pebble_granite>,<item:extcaves:pebble_granite>],[<item:extcaves:pebble_granite>,<item:extcaves:pebble_granite>,<item:extcaves:pebble_granite>]]);

craftingTable.removeByRegex(".*dirtstone.*");
craftingTable.removeByRegex(".*lavastone.*");
craftingTable.removeByRegex(".*oldstone.*");
craftingTable.removeByRegex(".*sediment.*");
stoneCutter.removeByRegex(".*dirtstone.*");
stoneCutter.removeByRegex(".*lavastone.*");
stoneCutter.removeByRegex(".*oldstone.*");
stoneCutter.removeByRegex(".*sediment.*");
