// MapperBase
mods.jei.JEI.hideRegex(".*mapperbase.*");

// Inspirations
mods.jei.JEI.hideItem(<item:inspirations:barometer>);
mods.jei.JEI.hideItem(<item:inspirations:photometer>);
mods.jei.JEI.hideItem(<item:inspirations:pipe>);
mods.jei.JEI.hideItem(<item:inspirations:collector>);
mods.jei.JEI.hideItem(<item:inspirations:cactus_seeds>);
mods.jei.JEI.hideItem(<item:inspirations:sugar_cane_seeds>);
mods.jei.JEI.hideItem(<item:inspirations:heartbeet>);
mods.jei.JEI.hideItem(<item:inspirations:beetroot_soup_bucket>);
mods.jei.JEI.hideItem(<item:inspirations:honey_bucket>);
mods.jei.JEI.hideItem(<item:inspirations:mushroom_stew_bucket>);
mods.jei.JEI.hideItem(<item:inspirations:potato_soup_bucket>);
mods.jei.JEI.hideItem(<item:inspirations:rabbit_stew_bucket>);
mods.jei.JEI.hideItem(<item:inspirations:potato_soup>);
mods.jei.JEI.hideItem(<item:inspirations:north_compass>);
mods.jei.JEI.hideRegex(".*waypoint_compass.*");
mods.jei.JEI.hideRegex(".*enlightened_bush.*");

mods.jei.JEI.hideFluid(<fluid:inspirations:beetroot_soup>);
mods.jei.JEI.hideFluid(<fluid:inspirations:flowing_beetroot_soup>);
mods.jei.JEI.hideFluid(<fluid:inspirations:honey>);
mods.jei.JEI.hideFluid(<fluid:inspirations:flowing_honey>);
mods.jei.JEI.hideFluid(<fluid:inspirations:milk>);
mods.jei.JEI.hideFluid(<fluid:inspirations:flowing_milk>);
mods.jei.JEI.hideFluid(<fluid:inspirations:mushroom_stew>);
mods.jei.JEI.hideFluid(<fluid:inspirations:flowing_mushroom_stew>);
mods.jei.JEI.hideFluid(<fluid:inspirations:potato_soup>);
mods.jei.JEI.hideFluid(<fluid:inspirations:flowing_potato_soup>);
mods.jei.JEI.hideFluid(<fluid:inspirations:rabbit_stew>);
mods.jei.JEI.hideFluid(<fluid:inspirations:flowing_rabbit_stew>);

mods.jei.JEI.hideCategory("inspirations:cauldron");

// Extended Caves
mods.jei.JEI.hideRegex(".*dirtstone.*");
mods.jei.JEI.hideRegex(".*lavastone.*");
mods.jei.JEI.hideRegex(".*marlstone.*");
mods.jei.JEI.hideRegex(".*oldstone.*");
mods.jei.JEI.hideRegex(".*sedimentstone.*");
mods.jei.JEI.hideItem(<item:extcaves:rock_flint>);
mods.jei.JEI.hideItem(<item:extcaves:pebble_andesite>);
mods.jei.JEI.hideItem(<item:extcaves:pebble_diorite>);
mods.jei.JEI.hideItem(<item:extcaves:pebble_granite>);

// Embellish Craft
mods.jei.JEI.hideRegex(".*embellishcraft.*basalt.*");
mods.jei.JEI.hideRegex(".*embellishcraft.*marble.*");

// Create
mods.jei.JEI.hideItem(<item:create:dough>);
mods.jei.JEI.hideCategory("create:automatic_shaped");
mods.jei.JEI.hideCategory("create:automatic_shapeless");

// Improved Stations
mods.jei.JEI.hideItem(<item:improved-stations:blast_furnace_slab>);
mods.jei.JEI.hideItem(<item:improved-stations:crafting_table_slab>);
mods.jei.JEI.hideItem(<item:improved-stations:furnace_slab>);
mods.jei.JEI.hideItem(<item:improved-stations:jukebox_slab>);
mods.jei.JEI.hideItem(<item:improved-stations:loom_slab>);
mods.jei.JEI.hideItem(<item:improved-stations:smoker_slab>);

// I Like Wood
mods.jei.JEI.hideItem(<item:ilikewood:oak_crafting_table>);
mods.jei.JEI.hideRegex(".*ilikewood.*axe.*");
mods.jei.JEI.hideRegex(".*ilikewood.*bow.*");
mods.jei.JEI.hideRegex(".*ilikewood.*hoe.*");
mods.jei.JEI.hideRegex(".*ilikewood.*shovel.*");
mods.jei.JEI.hideRegex(".*ilikewood.*sword.*");

mods.jei.JEI.hideItem(<item:ilikewood:acacia_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:acacia_ladder>);
mods.jei.JEI.hideItem(<item:ilikewood:birch_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:birch_ladder>);
mods.jei.JEI.hideItem(<item:ilikewood:crimson_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:crimson_ladder>);
mods.jei.JEI.hideItem(<item:ilikewood:dark_oak_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:dark_oak_ladder>);
mods.jei.JEI.hideItem(<item:ilikewood:jungle_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:jungle_ladder>);
mods.jei.JEI.hideItem(<item:ilikewood:oak_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:oak_ladder>);
mods.jei.JEI.hideItem(<item:ilikewood:spruce_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:spruce_ladder>);
mods.jei.JEI.hideItem(<item:ilikewood:warped_chest>);
mods.jei.JEI.hideItem(<item:ilikewood:warped_ladder>);

// Adorn
mods.jei.JEI.hideRegex(".*adorn.*crate.*");

// Better End
mods.jei.JEI.hideItem(<item:betterendforge:end_stone_smelter>);
mods.jei.JEI.hideItem(<item:betterendforge:guidebook>);
mods.jei.JEI.hideRegex(".*betterendforge.*hammer.*");
mods.jei.JEI.hideCategory("betterendforge:alloying");
mods.jei.JEI.hideCategory("betterendforge:anvil_smithing");

// Macaw's Bridges
mods.jei.JEI.hideItem(<item:mcwbridges:iron_platform>);

// Better Animals Plus
mods.jei.JEI.hideItem(<item:betteranimalsplus:lamprey_bucket>);
mods.jei.JEI.hideItem(<item:betteranimalsplus:lamprey_spawn_egg>);

// Quark
mods.jei.JEI.hideItem(<item:quark:gravisand>);
mods.jei.JEI.hideItem(<item:quark:sturdy_stone>);
mods.jei.JEI.hideRegex(".*speleothem.*");
mods.jei.JEI.hideRegex(".*quark.*snow.*");
mods.jei.JEI.hideRegex(".*quark.*limestone.*");
mods.jei.JEI.hideRegex(".*quark.*slate.*");
mods.jei.JEI.hideRegex(".*quark.*basalt.*");
mods.jei.JEI.hideRegex(".*quark.*myalite.*");
mods.jei.JEI.hideRegex(".*quark.*brimstone.*");
mods.jei.JEI.hideRegex(".*quark.*cobbedstone.*");
mods.jei.JEI.hideRegex(".*quark.*biotite.*");
mods.jei.JEI.hideRegex(".*quark.*midori.*");
mods.jei.JEI.hideRegex(".*quark.*prismarine.*");

mods.jei.JEI.hideItem(<item:quark:blaze_lantern>);
mods.jei.JEI.hideItem(<item:quark:chute>);
mods.jei.JEI.hideItem(<item:quark:ender_watcher>);
mods.jei.JEI.hideItem(<item:quark:magnet>);
mods.jei.JEI.hideItem(<item:quark:gold_bars>);
mods.jei.JEI.hideItem(<item:quark:iron_rod>);
mods.jei.JEI.hideRegex(".*quark.*blossom.*");
mods.jei.JEI.hideRegex(".*quark.*turf.*");
mods.jei.JEI.hideRegex(".*quark.*bonded.*");
mods.jei.JEI.hideRegex(".*quark.*cactus.*");
mods.jei.JEI.hideRegex(".*quark.*bookshelf.*");
mods.jei.JEI.hideRegex(".*quark.*stained.*");
mods.jei.JEI.hideRegex(".*quark.*:vertical.*");
mods.jei.JEI.hideRegex(".*quark.*item_frame.*");
mods.jei.JEI.hideRegex(".*quark.*slime_block.*");
mods.jei.JEI.hideRegex(".*quark.*crystal.*");
mods.jei.JEI.hideRegex(".*quark.*glowshroom.*");

mods.jei.JEI.hideItem(<item:quark:pickarang>);
mods.jei.JEI.hideItem(<item:quark:flamerang>);
mods.jei.JEI.hideItem(<item:quark:backpack>);
mods.jei.JEI.hideItem(<item:quark:ravager_hide>);
mods.jei.JEI.hideItem(<item:quark:rope>);
mods.jei.JEI.hideItem(<item:quark:forgotten_hat>);
mods.jei.JEI.hideRegex(".*quark.*frog.*");
mods.jei.JEI.hideRegex(".*quark.*crab.*");
mods.jei.JEI.hideRegex(".*quark.*egg.*");
mods.jei.JEI.hideRegex(".*quark.*soul.*");
mods.jei.JEI.hideRegex(".*quark.*tallow.*");
mods.jei.JEI.hideRegex(".*quark.*candle.*");

