# CreateCorner Modpack for Minecraft 1.16.4

Assembled by @Zarkith  
Presented by @SimonRoth

## This is a PRIVATE modpack. DO NOT REDISTRIBUTE IT!!

## HOW TO INSTALL:
	
1) Install Minecraft 1.16.4 (as a separate install from latest release).
    - After you've launched it once, you can delete the custom install from the launcher.
2) [Install Forge 35.1.36](http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.16.4.html).
    - If you're having trouble opening the .jar, you may have to reinstall Java.
3) Create a new custom install for this modpack in the Minecraft launcher.  
    <img src="https://i.imgur.com/EHu1IEE.png"  width="700" height="450">
    <img src="https://i.imgur.com/aUEVaLg.png"  width="700" height="450">
    - It doesn't matter what you name it or where it is, so long as the version of Minecraft it's using is 1.16.4-Forge-35.1.36
    - Expand "More Options" for the install, and change `-Xmx2G` to at LEAST `4G`, preferably `6G` or `8G`
      - Modded Minecraft is much heavier on RAM usage than Vanilla, so increasing this number will increase the maximum amount of RAM available to Minecraft. More RAM = Less freezing/stuttering!
4) Download the most recent .zip package from the [releases page](https://gitlab.com/createcorner/official-modpack/-/releases) and Extract the contents to your forge installation's folder.
    - If you decide to create any singleplayer worlds with this modpack, the `I Like Wood` datapack has been provided to allow its Biomes O'Plenty items to be craftable.

And that's it, you're all set up and good to go!

## IMPORTANT INFO:

Default config options and keybindings have been provided for you, but
feel free to change them as you see fit. Everything should work just fine
out of the box, with **ONE NOTABLE EXCEPTION**:	`Simple Voice Chat`

By default, `X` should open the Voice Chat settings menu. Here, you can set your default audio devices, adjust the always on threshold, etc. We STRONGLY advise heavily raising the dB threshold, as this voice chat mod **DOES NOT INCLUDE ANY NOISE CANCELLATION**!!

You don't have to use voice chat if you don't want to, but if you do, remember this; Changing ResourcePacks, Shaderpacks, or teleporting between dimensions via commands while you're using Always On voice will prevent you from being able to speak at all until you disconnect from and reconnect to the server! Travelling between dimensions via portals will not break it, but VC doesn't travel through them.

Speaking of portals, some Shaderpacks may crash if you look at them funny, so be careful with those.

## MOD LIST [v1.0]	(Excluding dependencies)

○ Create 0.3 • Quark • Tetra • Biomes O'Plenty • Pam's Harvestcraft 2 • Adorn	• Better End • More Red	• Bountiful • EmbellishCraft (+ Biomes O'Plenty Addon) • Inspirations	• GraveStone • Wrench • Extended Caves • Extended Lights • Macaw's Bridges, Roofs, Doors, & Paintings • Dramatic Doors • Valhelsia Structures • I Like Wood • Forgery • Additional Bars	• Better Animals Plus • Torch Slabs • KleeSlabs • Snow! Real Magic!	• Snow Under Trees • Extra Boats • Tetranomicon • Better Burning • Random Patches • Immersive Portals
• Clumps • Carry On	• Improved Stations	• Fast Workbench • Upstream	• Snow Under Trees • More Powerful Game Rules • CraftTweaker • CreateTweaker • ContentTweaker • JEI Tweaker

### CLIENTSIDE MODS: (Any of these may be safely removed)

• Just Enough Items (JEI) • Here's What You're Looking At (HWYLA) • OptiFine	• Emojiful • Item Zoom • Dynamic Surroundings • Ambient Environment • Apple Skin • Better Ping Display • Cat Jammies • Xaero's World Map & Minimap (Also included is the BSL shaderpack, which offers the best results for this pack out of all tested shaders).
